import requests
import logging
from vl_api_client.exceptions import ConnectionError, VirtualLibraryError
from properties.properties import VL_API_URL


logger = logging.getLogger('vl_front')


class BaseClient:
    def __init__(self):
        self.session = requests.Session()
        self.base_url = VL_API_URL

    def _response(self, url: str, method="GET", data=None):
        """ Uses HTTP GET, POST, PUT or DELETE

        Args:
            url (str): The url to use
            request_type (str): The HTTP method to use - GET, POST, PUT or DELETE
            data (dict): (optional) The data insert

        Returns:
            Request (object): if successful, the response from the server
        """

        try:
            logger.info(f"Virtual Libarry request: {url}")
            logger.debug("Virtual Libarry data: " + str(data))
            if method == "GET":
                response = requests.get(url=url, data=data)
            elif method == "POST":
                response = requests.post(url=url, data=data)
            elif method == "DELETE":
                response = requests.delete(url=url, data=data)
            else:
                response = requests.put(url=url, data=data)
        except (requests.exceptions.RequestException) as e:
            logger.error("Virtual Libarry connection error: " + repr(e))
            raise ConnectionError("Unable to connect to the Virtual Library API")
        except Exception as e:
            logger.critical(f"Virtual Libarry error: {repr(e)}")
            raise VirtualLibraryError(f"Virtual Library error: {repr(e)}")

        return response
