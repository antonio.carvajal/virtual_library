import logging

from typing import List
from properties.properties import CODE_ERRORS
from vl_api_client.baseclient import BaseClient
from vl_api_client.exceptions import NotFoundError, APIEError, ElementExistsError, NotEnoughCopiesError
from my_vl_app.models.bookshelf import Bookshelf
from my_vl_app.models.autor import Autor
from my_vl_app.models.book import Book
from my_vl_app.models.editorial import Editorial


logger = logging.getLogger('vl_front')


class VirtualLibraryClient(BaseClient):

    def __init__(self):
        super().__init__()

    # Bookshelfs calls

    def get_bookshelfs(self) -> List[Bookshelf]:
        url = f"http://{self.base_url}bookshelf/"
        response = self._response(url)
        bookshelfs: List[Bookshelf] = response.json()["data"] if response else []
        logger.info("getting bookshelfs")
        return bookshelfs

    def get_bookshelf(self, bookshelf_id: int) -> Bookshelf:
        url = f"http://{self.base_url}bookshelf/{bookshelf_id}/"
        response = self._response(url)

        if response.status_code == 404:
            raise NotFoundError(f"Bookshelf with id {bookshelf_id} not found")

        bookshelf: Bookshelf = response.json()["data"]
        logger.info(f"getting bookshelf with id {bookshelf_id}")
        return bookshelf

    def add_bookshelf(self, category: str):
        url = f"http://{self.base_url}bookshelf/?category={category}"
        response = self._response(url, method='POST')

        if response.status_code == 500:
            logger.error(f"Bookshelf with category {category} could not be added")
            raise APIEError(f"Bookshelf with category {category} could not be added")

        logger.info(f"adding bookshelf with category {category}")

    def delete_bookshelf(self, bookshelf_id: int):
        url = f"http://{self.base_url}bookshelf/{bookshelf_id}/"
        response = self._response(url, method='DELETE')

        if response.status_code == 404:
            logger.error(f"Bookshelf with id {bookshelf_id} not found")
            raise NotFoundError(f"Bookshelf with id {bookshelf_id} not found")
        elif response.status_code == 500:
            logger.error(f"Bookshelf with id {bookshelf_id} could not be deleted")
            raise APIEError(f"Bookshelf with id {bookshelf_id} could not be deleted")

        logger.info(f"deleting bookshelf with id {bookshelf_id}")

    def update_bookshelf(self, bookshelf_id: int, category: str):
        url = f"http://{self.base_url}bookshelf/{bookshelf_id}/?category={category}"
        response = self._response(url, method='PUT')

        if response.status_code == 404:
            logger.error(f"Bookshelf with id {bookshelf_id} not found")
            raise NotFoundError(f"Bookshelf with id {bookshelf_id} not found")
        elif response.status_code == 500:
            logger.error(f"Bookshelf with id {bookshelf_id} could not be updated")
            raise APIEError(f"Bookshelf with id {bookshelf_id} could not be updated")

        logger.info(f"updating bookshelf with id {bookshelf_id}")

    def add_book_to_bookshelf(self, book_id: int, bookshelf_id: int):
        url = f"http://{self.base_url}bookshelf/{bookshelf_id}/book/{book_id}/"
        response = self._response(url, method='POST')

        if response.status_code == 500:
            logger.error(f"Book with id '{book_id}' could not be added to bookshelf with id '{bookshelf_id}'")
            raise APIEError(f"Book with id '{book_id}' could not be added to bookshelf with id '{bookshelf_id}'")
        if response.status_code == 404:
            logger.error(f"Book with id '{book_id}' or bookshelf with id '{bookshelf_id}' not found")
            raise NotFoundError(f"Book with id '{book_id}' or bookshelf with id '{bookshelf_id}' not found")
        if response.status_code == 400 and response.json()["code"] == CODE_ERRORS["exists_code_error"]:
            logger.error(f"Book with id '{book_id}' already exists in bookshelf '{bookshelf_id}'")
            raise ElementExistsError(f"Book with id '{book_id}' already exists in bookshelf '{bookshelf_id}'")

        logger.info(f"adding book with id '{book_id}' to bookshelf '{bookshelf_id}'")

    def remove_book_from_bookshelf(self, book_id: int, bookshelf_id: int):
        url = f"http://{self.base_url}bookshelf/{bookshelf_id}/book/{book_id}/"
        response = self._response(url, method='DELETE')

        if response.status_code == 404:
            logger.error(f"Book with id '{book_id}' or bookshelf with id '{bookshelf_id}' not found")
            raise NotFoundError(f"Book with id '{book_id}' or bookshelf with id '{bookshelf_id}' not found")
        if response.status_code == 500:
            logger.error(f"Book with id '{book_id}' could not be deleted from bookshelf '{bookshelf_id}'")
            raise APIEError(f"Book with id '{book_id}' could not be deleted from bookshelf '{bookshelf_id}'")

        logger.info(f"removing book with id '{book_id}' from bookshelf '{bookshelf_id}'")

    # Editorial calls

    def get_all_editorials(self) -> List[Editorial]:
        url = f"http://{self.base_url}editorial/"
        response = self._response(url)
        editorials: List[Editorial] = response.json()["data"] if response else []
        logger.info("getting editorials")
        return editorials

    def get_editorial(self, name: str) -> Editorial:
        url = f"http://{self.base_url}editorial/{name}/"
        response = self._response(url)

        if response.status_code == 404:
            raise NotFoundError(f"Editorial with name {name} not found")

        editorial: Editorial = response.json()["data"]
        logger.info(f"getting editorial with name {name}")
        return editorial

    def add_editorial(self, editorial: Editorial):
        url = f"http://{self.base_url}editorial/"
        response = self._response(url, method='POST', data=editorial.json())

        if response.status_code == 500:
            logger.error(f"Editorial with name {editorial.name} could not be added")
            raise APIEError(f"Editorial with name {editorial.name} could not be added")
        if response.status_code == 400 and response.json()["code"] == CODE_ERRORS["exists_code_error"]:
            logger.error(f"Editorial with name {editorial.name} already exists")
            raise ElementExistsError(f"Editorial with name {editorial.name} already exists")

        logger.info(f"adding editorial with name {editorial.name}")

    def delete_editorial(self, name: str):
        url = f"http://{self.base_url}editorial/{name}/"
        response = self._response(url, method='DELETE')

        if response.status_code == 404:
            logger.error(f"Editorial with name {name} not found")
            raise NotFoundError(f"Editorial with name {name} not found")
        elif response.status_code == 400 and response.json()["code"] == CODE_ERRORS["exists_code_error"]:
            logger.error(f"Editorial with name {name} could not be deleted because it has books")
            raise ElementExistsError(f"Editorial with name {name} could not be deleted because it has books")
        elif response.status_code == 500:
            logger.error(f"Editorial with name {name} could not be deleted")
            raise APIEError(f"Editorial with name {name} could not be deleted")

        logger.info(f"deleting editorial with name {name}")

    def update_editorial(self, name: str, editorial: Editorial):
        url = f"http://{self.base_url}editorial/{name}/"
        response = self._response(url, method='PUT', data=editorial.json())

        if response.status_code == 404:
            logger.error(f"Editorial '{name}' not found")
            raise NotFoundError(f"Editorial '{name}' not found")
        elif response.status_code == 400 and response.json()["code"] == CODE_ERRORS["exists_code_error"]:
            logger.error(f"Editorial '{name}' could not be updated because the editorial '{editorial.name}' already exists")
            raise ElementExistsError(f"Editorial '{name}' could not be updated because the editorial '{editorial.name}' already exists")
        elif response.status_code == 500:
            logger.error(f"Editorial '{name}' could not be updated")
            raise APIEError(f"Editorial '{name}' could not be updated")

        logger.info(f"updating editorial with the new name: '{editorial.name}'")

    # Authors calls

    def get_all_authors(self) -> List[Autor]:
        url = f"http://{self.base_url}autor/"
        response = self._response(url)
        authors: List[Autor] = [Autor(name=data['name']) for data in response.json()["data"]] if response else []

        for author in authors:
            url = f"http://{self.base_url}autor/{author.name}/books/"
            response = self._response(url)
            author.books = response.json()["data"] if response else []

        logger.info("getting authors")
        return authors

    def get_author(self, name: str) -> Autor:
        url = f"http://{self.base_url}autor/{name}/"
        response = self._response(url)

        if response.status_code == 404:
            raise NotFoundError(f"Author with name {name} not found")

        author: Autor = Autor(name=response.json()["data"]["name"])
        url = f"http://{self.base_url}autor/{author.name}/books/"
        response = self._response(url)
        author.books = response.json()["data"] if response else []
        logger.info(f"getting author with name {name}")
        return author

    def add_author(self, author: Autor):
        url = f"http://{self.base_url}autor/?name={author.name}"
        response = self._response(url, method='POST')

        if response.status_code == 500:
            logger.error(f"Author with name {author.name} could not be added")
            raise APIEError(f"Author with name {author.name} could not be added")
        if response.status_code == 400 and response.json()["code"] == CODE_ERRORS["exists_code_error"]:
            logger.error(f"Author with name {author.name} already exists")
            raise ElementExistsError(f"Author with name {author.name} already exists")

        logger.info(f"adding author with name {author.name}")

    def delete_author(self, name: str):
        url = f"http://{self.base_url}autor/{name}/"
        response = self._response(url, method='DELETE')

        if response.status_code == 404:
            logger.error(f"Author with name {name} not found")
            raise NotFoundError(f"Author with name {name} not found")
        elif response.status_code == 400 and response.json()["code"] == CODE_ERRORS["exists_code_error"]:
            logger.error(f"Author with name {name} could not be deleted because it has books")
            raise ElementExistsError(f"Author with name {name} could not be deleted because it has books")
        elif response.status_code == 500:
            logger.error(f"Author with name {name} could not be deleted")
            raise APIEError(f"Author with name {name} could not be deleted")

        logger.info(f"deleting author with name {name}")

    def update_author(self, name: str, author: Autor):
        url = f"http://{self.base_url}autor/{name}/"
        response = self._response(url, method='PUT', data=author.json())

        if response.status_code == 404:
            logger.error(f"Author with name {name} not found")
            raise NotFoundError(f"Author with name {name} not found")
        elif response.status_code == 400 and response.json()["code"] == CODE_ERRORS["exists_code_error"]:
            logger.error(f"Author with name {name} could not be updated because the author '{author.name}' already exists")
            raise ElementExistsError(f"Author with name {name} could not be updated because the author '{author.name}' already exists")
        elif response.status_code == 500:
            logger.error(f"Author with name {name} could not be updated")
            raise APIEError(f"Author with name {name} could not be updated")

        logger.info(f"updating author with the new name: '{author.name}'")

    # Books calls

    def get_all_books(self) -> List[Book]:
        url = f"http://{self.base_url}book/"
        response = self._response(url)
        books: List[Book] = [
                Book(
                    id=data['id'],
                    title=data['title'],
                    synopsis=data["synopsis"],
                    category=data["category"],
                    autor=data["autor"],
                    editorial=data["editorial"],
                    copies=data["copies"]) for data in response.json()["data"]] if response else []
        logger.info("getting books")
        return books

    def get_book(self, book_id: int) -> Book:
        url = f"http://{self.base_url}book/{book_id}/"
        response = self._response(url)

        if response.status_code == 404:
            raise NotFoundError(f"Book with id {book_id} not found")

        data = response.json()["data"]

        book: Book = Book(
            id=data["id"],
            title=data["title"],
            synopsis=data["synopsis"],
            category=data["category"],
            autor=data["autor"],
            editorial=data["editorial"],
            copies=data["copies"])
        logger.info(f"getting book with id {book_id}")
        return book

    def add_book(self, book: Book):
        url = f"http://{self.base_url}book/"
        response = self._response(url, method='POST', data=book.json())

        if response.status_code == 500:
            logger.error(f"Book with title {book.title} could not be added")
            raise APIEError(f"Book with title {book.title} could not be added")
        if response.status_code == 400 and response.json()["code"] == CODE_ERRORS["exists_code_error"]:
            logger.error(f"Book with title {book.title} already exists")
            raise ElementExistsError(f"Book with title {book.title} already exists")

        logger.info(f"adding book with title {book.title}")

    def delete_book(self, book_id: int):
        url = f"http://{self.base_url}book/{book_id}/"
        response = self._response(url, method='DELETE')

        if response.status_code == 404:
            logger.error(f"Book with id {book_id} not found")
            raise NotFoundError(f"Book with id {book_id} not found")
        elif response.status_code == 500:
            logger.error(f"Book with id {book_id} could not be deleted")
            raise APIEError(f"Book with id {book_id} could not be deleted")

        logger.info(f"deleting book with id {book_id}")

    def update_book(self, book_id: int, book: Book):
        url = f"http://{self.base_url}book/{book_id}/"
        response = self._response(url, method='PUT', data=book.json())

        if response.status_code == 404:
            logger.error(f"Book with id {book_id} not found")
            raise NotFoundError(f"Book with id {book_id} not found")
        elif response.status_code == 400 and response.json()["code"] == CODE_ERRORS["exists_code_error"]:
            logger.error(f"Book with title '{book.title}' could not be updated because the book \
                          '{book.title}' already exists for the '{book.editorial.name}' editorial")
            raise ElementExistsError(f"Book with title '{book.title}' could not be updated because the book \
                                      '{book.title}' already exists for the '{book.editorial.name}' editorial")
        elif response.status_code == 500:
            logger.error(f"Book with id {book_id} could not be updated")
            raise APIEError(f"Book with id {book_id} could not be updated")

        logger.info(f"updating book with the new title: '{book.title}'")

    def add_book_copies(self, book_id: int, copies: int):
        url = f"http://{self.base_url}book/{book_id}/copies/add/?copies={copies}"
        response = self._response(url, method='PUT')

        if response.status_code == 500:
            logger.error(f"Book with id {book_id} could not be updated")
            raise APIEError(f"Book with id {book_id} could not be updated")
        if response.status_code == 404:
            logger.error(f"Book with id {book_id} not found")
            raise NotFoundError(f"Book with id {book_id} not found")

        logger.info(f"adding {copies} copies to book with id {book_id}")

    def remove_book_copies(self, book_id: int, copies: int):
        url = f"http://{self.base_url}book/{book_id}/copies/remove/?copies={copies}"
        response = self._response(url, method='PUT')

        if response.status_code == 500:
            logger.error(f"Book with id {book_id} could not be updated")
            raise APIEError(f"Book with id {book_id} could not be updated")
        if response.status_code == 404:
            logger.error(f"Book with id {book_id} not found")
            raise NotFoundError(f"Book with id {book_id} not found")
        if response.status_code == 400 and response.json()["code"] == CODE_ERRORS["update_code_error"]:
            logger.error(f"Book with id {book_id} does not have enough copies")
            raise NotEnoughCopiesError(f"Book with id {book_id} does not have enough copies")

        logger.info(f"adding {copies} copies to book with id {book_id}")
