class VirtualLibraryError(Exception):
    def __init__(self, message: str, code: int = 500) -> None:
        super().__init__()
        self.message = message
        self.code = code


class ConnectionError(VirtualLibraryError):
    def __init__(self, message: str) -> None:
        super().__init__(message, 500)


class NotFoundError(VirtualLibraryError):
    def __init__(self, message: str) -> None:
        super().__init__(message, 404)


class APIEError(VirtualLibraryError):
    def __init__(self, message: str) -> None:
        super().__init__(message, 500)


class ElementExistsError(VirtualLibraryError):
    def __init__(self, message: str) -> None:
        super().__init__(message, 400)


class NotEnoughCopiesError(VirtualLibraryError):
    def __init__(self, message: str) -> None:
        super().__init__(message, 400)
