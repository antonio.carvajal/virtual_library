from django.urls import path

from .views import bookshelfviews, editorialviews, authorviews, bookviews

urlpatterns = [
    path('', bookshelfviews.home, name='home'),
    path('<int:bookshelf_id>/', bookshelfviews.get_bookshelf, name='bookshelf_detail'),
    path('add/', bookshelfviews.add_bookshelf, name='add_bookshelf'),
    path('delete/', bookshelfviews.delete_bookshelf, name='delete_bookshelf'),
    path('update/', bookshelfviews.update_bookshelf, name='update_bookshelf'),

    path('editorial/', editorialviews.all_editorials, name='all_editorials'),
    path('editorial/add/', editorialviews.add_editorial, name='add_editorial'),
    path('editorial/delete/', editorialviews.delete_editorial, name='delete_editorial'),
    path('editorial/update/', editorialviews.update_editorial, name='update_editorial'),
    path('editorial/<str:editorial_name>/', editorialviews.get_editorial, name='editorial_detail'),

    path('author/', authorviews.all_authors, name='all_authors'),
    path('author/add/', authorviews.add_author, name='add_author'),
    path('author/delete/', authorviews.delete_author, name='delete_author'),
    path('author/update/', authorviews.update_author, name='update_author'),
    path('author/<str:author_name>/', authorviews.get_author, name='author_detail'),

    path('book/', bookviews.all_books, name='all_books'),
    path('book/add/', bookviews.add_book, name='add_book'),
    path('book/delete/', bookviews.delete_book, name='delete_book'),
    path('book/update/', bookviews.update_book, name='update_book'),
    path('book/add/copies/', bookviews.add_copies, name='add_copies_book'),
    path('book/remove/copies/', bookviews.remove_copies, name='remove_copies_book'),
    path('book/add/bookshelf/', bookviews.add_book_to_bookshelf, name='add_book_bookshelf'),
    path('book/remove/bookshelf/', bookviews.remove_book_from_bookshelf, name='remove_book_bookshelf'),
    path('book/<int:book_id>/', bookviews.get_book, name='book_detail'),
]
