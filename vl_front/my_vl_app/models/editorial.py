from pydantic import BaseModel


class Editorial(BaseModel):
    """Editorial class that represent a editorial"""
    name: str
    address: str
    phone: str
