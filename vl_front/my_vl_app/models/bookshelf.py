from typing import List, Optional
from pydantic import BaseModel

from my_vl_app.models.book import Book, Category


class Bookshelf(BaseModel):
    """Bookshelf class that represent a bookshelf"""
    id: int
    category: Category
    books: Optional[List[Book]] = []
