from pydantic import BaseModel
from typing import List, Optional


class Autor(BaseModel):
    """Autor class that represent a autor"""
    name: str
    books: Optional[List] = []
