from pydantic import BaseModel
from enum import Enum
from typing import Optional

from my_vl_app.models.autor import Autor
from my_vl_app.models.editorial import Editorial


class Category(str, Enum):
    """Request status type model"""
    FANTASY = "Fantasy"
    ACTION = "Action"
    SCI_FI = "sci-fi"
    POETRY = "poetry"
    DRAMA = "drama"


class Book(BaseModel):
    """Book class that represent a book"""
    id: Optional[int] = 0
    title: str
    synopsis: Optional[str] = ""
    category: Category
    autor: Autor
    editorial: Editorial
    copies: Optional[int] = 1
