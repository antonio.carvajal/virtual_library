from django.apps import AppConfig


class MyVlAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'my_vl_app'
