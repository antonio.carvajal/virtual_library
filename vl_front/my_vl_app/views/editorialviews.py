from django.shortcuts import render

from typing import List
from vl_api_client.vlclient import VirtualLibraryClient
from my_vl_app.models.editorial import Editorial
from vl_api_client.exceptions import NotFoundError, APIEError, ElementExistsError


def all_editorials(request):
    client = VirtualLibraryClient()
    editorials: List[Editorial] = client.get_all_editorials()
    data = {'editorials': editorials, 'message': ""}

    if not editorials:
        data["message"] = "No editorials found in the Virtual Library"

    return render(request, "my_vl_app/editorial_home.html", data)


def get_editorial(request, editorial_name: str):
    client = VirtualLibraryClient()

    try:
        editorial: Editorial = client.get_editorial(editorial_name)
    except NotFoundError as nfe:
        return render(request, "my_vl_app/editorial_home.html", {
                      'editorials': [],
                      'message': nfe.message
                      })
    return render(request, "my_vl_app/editorial_home.html", {
                  'editorials': [editorial],
                  'message': ""
                  })


def add_editorial(request):
    data = {'control': 'pass'}

    if request.method == 'POST':
        try:
            client = VirtualLibraryClient()
            name = request.POST.get('inputName').strip()
            address = request.POST.get('inputAddress').strip()
            phone = request.POST.get('inputPhone').strip()
            editorial: Editorial = Editorial(
                    name=name,
                    address=address,
                    phone=phone
                    )
            client.add_editorial(editorial)
            data['control'] = 'added'
        except APIEError:
            data['control'] = 'error'
        except ElementExistsError:
            data['control'] = 'exists'

    return render(request, "my_vl_app/editorial_add.html", data)


def delete_editorial(request):
    client = VirtualLibraryClient()
    control = 'pass'

    if request.method == 'POST':
        try:
            editorial_name = request.POST.get('inputEditorial_name')
            client.delete_editorial(editorial_name)
            control = 'delete'
        except (APIEError, NotFoundError):
            control = 'error'
        except ElementExistsError:
            control = 'errorExists'

    editorials: List[Editorial] = client.get_all_editorials()
    data = {'editorials': editorials, 'control': control}
    return render(request, "my_vl_app/editorial_delete.html", data)


def update_editorial(request):
    client = VirtualLibraryClient()
    control = 'pass'
    data = {}

    if request.method == 'POST':
        try:
            editorial_name = request.POST.get('inputEditorial_name')
            new_name = request.POST.get('inputName').strip()
            address = request.POST.get('inputAddress').strip()
            phone = request.POST.get('inputPhone').strip()
            editorial: Editorial = Editorial(
                    name=new_name,
                    address=address,
                    phone=phone
                    )
            client.update_editorial(editorial_name, editorial)
            control = 'updated'
        except (APIEError, NotFoundError):
            control = 'error'
        except ElementExistsError as ee:
            control = 'errorExists'
            data.update({'message': ee.message})

    editorials: List[Editorial] = client.get_all_editorials()
    data.update({'editorials': editorials, 'control': control})
    return render(request, "my_vl_app/editorial_update.html", data)
