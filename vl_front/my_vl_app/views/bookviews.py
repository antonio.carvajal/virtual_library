from django.shortcuts import render

from typing import List
from vl_api_client.vlclient import VirtualLibraryClient
from my_vl_app.models.book import Book, Category
from my_vl_app.models.editorial import Editorial
from my_vl_app.models.bookshelf import Bookshelf
from my_vl_app.models.autor import Autor
from vl_api_client.exceptions import NotFoundError, APIEError, ElementExistsError, NotEnoughCopiesError


def all_books(request):
    client = VirtualLibraryClient()
    books: List[Book] = client.get_all_books()
    data = {'books': books, 'message': ""}

    if not books:
        data["message"] = "No books found in the Virtual Library"

    return render(request, "my_vl_app/book_home.html", data)


def get_book(request, book_id: int):
    client = VirtualLibraryClient()

    try:
        book: Book = client.get_book(book_id)
    except NotFoundError as nfe:
        return render(request, "my_vl_app/book_home.html", {
                      'books': [],
                      'message': nfe.message
                      })
    return render(request, "my_vl_app/book_home.html", {
                  'books': [book],
                  'message': ""
                  })


def add_book(request):
    client = VirtualLibraryClient()
    categories: List[str] = [category.value for category in Category]
    editorials: List[Editorial] = client.get_all_editorials()
    data = {'categories': categories, 'editorials': editorials, 'control': 'pass'}

    if request.method == 'POST':
        try:
            title: str = request.POST.get('inputTitle').strip()
            synopsis: str = request.POST.get('inputSynopsis').strip()
            category: Category = request.POST.get('inputCategory')
            author: Autor = Autor(name=request.POST.get('inputAuthor').strip())
            editorial: Editorial = client.get_editorial(name=request.POST.get('inputEditorial'))
            book: Book = Book(
                    title=title,
                    synopsis=synopsis,
                    category=category,
                    autor=author,
                    editorial=editorial)
            client.add_book(book)
            data['control'] = 'added'
        except APIEError:
            data['control'] = 'error'
        except ElementExistsError:
            data['control'] = 'exists'

    return render(request, "my_vl_app/book_add.html", data)


def delete_book(request):
    client = VirtualLibraryClient()
    control = 'pass'
    data = {}

    if request.method == 'POST':
        try:
            client = VirtualLibraryClient()
            book_id = request.POST.get('inputBook_id')
            client.delete_book(book_id)
            control = 'delete'
        except (APIEError, NotFoundError):
            control = 'error'

    books: List[Book] = client.get_all_books()
    data.update({'books': books, 'control': control})
    return render(request, "my_vl_app/book_delete.html", data)


def update_book(request):
    client = VirtualLibraryClient()
    control = 'pass'
    categories: List[str] = [category.value for category in Category]
    editorials: List[Editorial] = client.get_all_editorials()
    data = {'categories': categories, 'editorials': editorials}

    if request.method == 'POST':
        try:
            book_id: int = request.POST.get('inputBook_id')
            title: str = request.POST.get('inputTitle').strip()
            synopsis: str = request.POST.get('inputSynopsis').strip()
            category: Category = request.POST.get('inputCategory')
            author: Autor = Autor(name=request.POST.get('inputAuthor').strip())
            editorial: Editorial = client.get_editorial(name=request.POST.get('inputEditorial'))
            book: Book = Book(
                    title=title,
                    synopsis=synopsis,
                    category=category,
                    autor=author,
                    editorial=editorial)
            client.update_book(book_id, book)
            control = 'updated'
        except (APIEError, NotFoundError):
            control = 'error'
        except ElementExistsError as ee:
            control = 'errorExists'
            data.update({'message': ee.message})

    books: List[Book] = client.get_all_books()
    data.update({'books': books, 'control': control})
    return render(request, "my_vl_app/book_update.html", data)


def add_copies(request):
    client = VirtualLibraryClient()
    control = 'pass'
    data = {}

    if request.method == 'POST':
        try:
            book_id: int = request.POST.get('inputBook_id')
            copies: int = request.POST.get('inputCopies')
            client.add_book_copies(book_id, copies)
            control = 'updated'
        except (APIEError, NotFoundError):
            control = 'error'

    books: List[Book] = client.get_all_books()
    data.update({'books': books, 'control': control})
    return render(request, "my_vl_app/book_add_copies.html", data)


def remove_copies(request):
    client = VirtualLibraryClient()
    control = 'pass'
    data = {}

    if request.method == 'POST':
        try:
            book_id: int = request.POST.get('inputBook_id')
            copies: int = request.POST.get('inputCopies')
            client.remove_book_copies(book_id, copies)
            control = 'updated'
        except (APIEError, NotFoundError):
            control = 'error'
        except NotEnoughCopiesError:
            control = 'errorNotEnoughCopies'

    books: List[Book] = client.get_all_books()
    data.update({'books': books, 'control': control})
    return render(request, "my_vl_app/book_remove_copies.html", data)


def add_book_to_bookshelf(request):
    client = VirtualLibraryClient()
    control = 'pass'
    data = {}

    if request.method == 'POST':
        try:
            bookshelf_id: int = request.POST.get('inputBookshelf_id')
            book_id: int = request.POST.get('inputBook_id')
            client.add_book_to_bookshelf(book_id, bookshelf_id)
            control = 'added'
        except (APIEError, NotFoundError):
            control = 'error'
        except ElementExistsError:
            control = 'errorExists'

    books: List[Book] = client.get_all_books()
    bookshelfs: List[Bookshelf] = client.get_bookshelfs()
    data.update({'books': books, 'bookshelfs': bookshelfs, 'control': control})
    return render(request, "my_vl_app/book_add_to_bookshelf.html", data)


def remove_book_from_bookshelf(request):
    client = VirtualLibraryClient()
    control = 'pass'
    data = {}

    if request.method == 'POST':
        try:
            bookshelf_id: int = request.POST.get('inputBookshelf_id')
            book_id: int = request.POST.get('inputBook_id')
            client.remove_book_from_bookshelf(book_id, bookshelf_id)
            control = 'delete'
        except (APIEError):
            control = 'error'
        except NotFoundError:
            control = 'notFoundError'

    books: List[Book] = client.get_all_books()
    bookshelfs: List[Bookshelf] = client.get_bookshelfs()
    data.update({'books': books, 'bookshelfs': bookshelfs, 'control': control})
    return render(request, "my_vl_app/book_remove_from_bookshelf.html", data)
