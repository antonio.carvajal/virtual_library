from django.shortcuts import render

from typing import List
from vl_api_client.vlclient import VirtualLibraryClient
from my_vl_app.models.bookshelf import Bookshelf
from my_vl_app.models.book import Category
from vl_api_client.exceptions import NotFoundError, APIEError


def home(request):
    client = VirtualLibraryClient()
    bookshelfs: List[Bookshelf] = client.get_bookshelfs()
    data = {'bookshelfs': bookshelfs, 'message': ""}

    if not bookshelfs:
        data["message"] = "No bookshelfs found in the Virtual Library"

    return render(request, "my_vl_app/home.html", data)


def get_bookshelf(request, bookshelf_id: int):
    client = VirtualLibraryClient()

    try:
        bookshelf: Bookshelf = client.get_bookshelf(bookshelf_id)
    except NotFoundError as nfe:
        return render(request, "my_vl_app/home.html", {
                      'bookshelfs': [],
                      'message': nfe.message
                      })
    return render(request, "my_vl_app/home.html", {
                  'bookshelfs': [bookshelf],
                  'message': ""
                  })


def add_bookshelf(request):
    categories: List[str] = [category.value for category in Category]
    data = {'categories': categories, 'control': 'pass'}

    if request.method == 'POST':
        try:
            client = VirtualLibraryClient()
            category = request.POST.get('inputCategory')
            client.add_bookshelf(category)
        except APIEError:
            data['control'] = 'error'
        data['control'] = 'added'

    return render(request, "my_vl_app/bookshelf_add.html", data)


def delete_bookshelf(request):
    client = VirtualLibraryClient()
    control = 'pass'

    if request.method == 'POST':
        try:
            bookshelf_id = request.POST.get('inputBookshelf_id')
            client.delete_bookshelf(bookshelf_id)
            control = 'delete'
        except (APIEError, NotFoundError):
            control = 'error'

    bookshelfs: List[Bookshelf] = client.get_bookshelfs()
    data = {'bookshelfs': bookshelfs, 'control': control}
    return render(request, "my_vl_app/bookshelf_delete.html", data)


def update_bookshelf(request):
    client = VirtualLibraryClient()
    categories: List[str] = [category.value for category in Category]
    control = 'pass'

    if request.method == 'POST':
        try:
            bookshelf_id = request.POST.get('inputBookshelf_id')
            category = request.POST.get('inputCategory')
            client.update_bookshelf(bookshelf_id, category)
            control = 'updated'
        except (APIEError, NotFoundError):
            control = 'error'

    bookshelfs: List[Bookshelf] = client.get_bookshelfs()
    data = {'categories': categories, 'bookshelfs': bookshelfs, 'control': control}
    return render(request, "my_vl_app/bookshelf_update.html", data)
