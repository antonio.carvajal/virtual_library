from django.shortcuts import render

from typing import List
from vl_api_client.vlclient import VirtualLibraryClient
from my_vl_app.models.autor import Autor
from vl_api_client.exceptions import NotFoundError, APIEError, ElementExistsError


def all_authors(request):
    client = VirtualLibraryClient()
    authors: List[Autor] = client.get_all_authors()
    data = {'authors': authors, 'message': ""}

    if not authors:
        data["message"] = "No authors found in the Virtual Library"

    return render(request, "my_vl_app/author_home.html", data)


def get_author(request, author_name: str):
    client = VirtualLibraryClient()

    try:
        author: Autor = client.get_author(author_name)
    except NotFoundError as nfe:
        return render(request, "my_vl_app/author_home.html", {
                      'authors': [],
                      'message': nfe.message
                      })
    return render(request, "my_vl_app/author_home.html", {
                  'authors': [author],
                  'message': ""
                  })


def add_author(request):
    data = {'control': 'pass'}

    if request.method == 'POST':
        try:
            client = VirtualLibraryClient()
            name = request.POST.get('inputName').strip()
            author: Autor = Autor(name=name)
            client.add_author(author)
            data['control'] = 'added'
        except APIEError:
            data['control'] = 'error'
        except ElementExistsError:
            data['control'] = 'exists'

    return render(request, "my_vl_app/author_add.html", data)


def delete_author(request):
    client = VirtualLibraryClient()
    control = 'pass'
    data = {}

    if request.method == 'POST':
        try:
            author_name = request.POST.get('inputAuthor_name').strip()
            client.delete_author(author_name)
            control = 'delete'
        except (APIEError, NotFoundError):
            control = 'error'
        except ElementExistsError as ee:
            control = 'errorExists'
            message = ee.message
            data.update({'message': message})

    authors: List[Autor] = client.get_all_authors()
    data.update({'authors': authors, 'control': control})
    return render(request, "my_vl_app/author_delete.html", data)


def update_author(request):
    client = VirtualLibraryClient()
    control = 'pass'
    data = {}

    if request.method == 'POST':
        try:
            author_name = request.POST.get('inputAutor_name').strip()
            new_name = request.POST.get('inputName').strip()
            author: Autor = Autor(name=new_name)
            client.update_author(author_name, author)
            control = 'updated'
        except (APIEError, NotFoundError):
            control = 'error'
        except ElementExistsError as ee:
            control = 'errorExists'
            data.update({'message': ee.message})

    authors: List[Autor] = client.get_all_authors()
    data.update({'authors': authors, 'control': control})
    return render(request, "my_vl_app/author_update.html", data)
