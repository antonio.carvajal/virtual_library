import os

APP_PATH = os.getenv("APP_PATH", "./")

############################
# VL_API CLIENT PROPERTIES #
############################

# TODO Cambiar por vl_api la URL
VL_API_PORT = '8080'
VL_API_URL = os.getenv("VL_API_URL", f"127.0.0.1:{VL_API_PORT}/")


#####################
# LOGGER PROPERTIES #
#####################
LOG_CONF_PATH = os.path.join(APP_PATH, os.path.join('properties', 'log_config.yml'))

###################
# API CODE ERRORS #
###################
CODE_ERRORS = {
    "ok": 0,
    "not_found_code_error": 1,
    "obtain_problem_code_error": 2,
    "create_code_error": 3,
    "update_code_error": 4,
    "delete_code_error": 5,
    "exists_code_error": 6,
    "http_code_error": 7,
    "validation_code_error": 8
}
