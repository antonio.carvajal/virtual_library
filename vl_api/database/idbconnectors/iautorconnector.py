from abc import ABCMeta, abstractmethod
from typing import List

from api.models.autor import Autor


class IAutorConnector(metaclass=ABCMeta):

    @abstractmethod
    def set_autor_table_name(self, table_name: str):
        raise NotImplementedError

    @abstractmethod
    def create_autor_table(self):
        raise NotImplementedError

    @abstractmethod
    def autor_table_exists(self) -> bool:
        raise NotImplementedError

    @abstractmethod
    def drop_autor_table(self):
        raise NotImplementedError

    @abstractmethod
    def truncate_autor_table(self):
        raise NotImplementedError

    @abstractmethod
    def get_all_autors(self) -> List[Autor]:
        raise NotImplementedError

    @abstractmethod
    def get_autor(self, name: str) -> Autor:
        raise NotImplementedError

    @abstractmethod
    def get_autor_id(self, name: str) -> int:
        raise NotImplementedError

    @abstractmethod
    def update_autor(self, name: str, new_name: str):
        raise NotImplementedError

    @abstractmethod
    def insert_autor(self, name: str) -> int:
        raise NotImplementedError

    @abstractmethod
    def autor_exists(self, name: str) -> bool:
        raise NotImplementedError

    @abstractmethod
    def delete_autor(self, name: str):
        raise NotImplementedError
