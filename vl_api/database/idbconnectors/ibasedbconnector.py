from abc import ABCMeta, abstractmethod


class IBaseDBConnector(metaclass=ABCMeta):

    @abstractmethod
    def connect(self) -> None:
        raise NotImplementedError

    @abstractmethod
    def disconnect(self) -> None:
        raise NotImplementedError

    @abstractmethod
    def _run_query(self, query, _args=None) -> None:
        raise NotImplementedError

    @abstractmethod
    def is_up(self) -> bool:
        raise NotImplementedError

    @abstractmethod
    def get_info_blob(self) -> str:
        raise NotImplementedError
