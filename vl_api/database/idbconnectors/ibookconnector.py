from abc import ABCMeta, abstractmethod
from typing import List
from api.models.book import Book


class IBookConnector(metaclass=ABCMeta):

    @abstractmethod
    def set_book_table_name(self, table_name: str):
        raise NotImplementedError

    @abstractmethod
    def create_book_table(self):
        raise NotImplementedError

    @abstractmethod
    def book_table_exists(self) -> bool:
        raise NotImplementedError

    @abstractmethod
    def drop_book_table(self):
        raise NotImplementedError

    @abstractmethod
    def truncate_book_table(self):
        raise NotImplementedError

    @abstractmethod
    def get_book(self, id: int) -> Book:
        raise NotImplementedError

    @abstractmethod
    def get_all_books(self) -> List[Book]:
        raise NotImplementedError

    @abstractmethod
    def insert_book(self, book: Book) -> int:
        raise NotImplementedError

    @abstractmethod
    def book_exists_by_name_editorial(self, title: str, editorial_name: str) -> bool:
        raise NotImplementedError

    @abstractmethod
    def editorial_have_book(self, editorial_name: str) -> bool:
        raise NotImplementedError

    @abstractmethod
    def update_book(self, book_id: int, book: Book, only_copies: bool = False):
        raise NotImplementedError

    @abstractmethod
    def delete_book(self, id: int):
        raise NotImplementedError

    @abstractmethod
    def book_exists(self, id: int) -> bool:
        raise NotImplementedError
