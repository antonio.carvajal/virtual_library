from abc import ABCMeta, abstractmethod
from typing import List
from api.models.editorial import Editorial


class IEditorialConnector(metaclass=ABCMeta):

    @abstractmethod
    def set_editorial_table_name(self, table_name: str):
        raise NotImplementedError

    @abstractmethod
    def create_editorial_table(self):
        raise NotImplementedError

    @abstractmethod
    def editorial_table_exists(self):
        raise NotImplementedError

    @abstractmethod
    def drop_editorial_table(self):
        raise NotImplementedError

    @abstractmethod
    def truncate_editorial_table(self):
        raise NotImplementedError

    @abstractmethod
    def get_editorial(self, name: str) -> Editorial:
        raise NotImplementedError

    @abstractmethod
    def get_editorial_id(self, name: str) -> int:
        raise NotImplementedError

    @abstractmethod
    def get_editorial_by_id(self, id: int) -> Editorial:
        raise NotImplementedError

    @abstractmethod
    def get_all_editorials(self) -> List[Editorial]:
        raise NotImplementedError

    @abstractmethod
    def insert_editorial(self, editorial: Editorial):
        raise NotImplementedError

    @abstractmethod
    def update_editorial(self, name: str, editorial: Editorial):
        raise NotImplementedError

    @abstractmethod
    def delete_editorial(self, name: str):
        raise NotImplementedError

    @abstractmethod
    def editorial_exists(self, name: str) -> bool:
        raise NotImplementedError
