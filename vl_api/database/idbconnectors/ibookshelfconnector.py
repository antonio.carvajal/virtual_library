from abc import ABCMeta, abstractmethod
from typing import List
from api.models.bookshelf import Bookshelf
from api.models.book import Category


class IBookshelfConnector(metaclass=ABCMeta):

    @abstractmethod
    def set_bookshelf_table_name(self, table_name: str):
        raise NotImplementedError

    @abstractmethod
    def create_bookshelf_table(self):
        raise NotImplementedError

    @abstractmethod
    def bookshelf_table_exists(self) -> bool:
        raise NotImplementedError

    @abstractmethod
    def drop_bookshelf_table(self):
        raise NotImplementedError

    @abstractmethod
    def truncate_bookshelf_table(self):
        raise NotImplementedError

    @abstractmethod
    def get_bookshelf(self, id: int) -> Bookshelf:
        raise NotImplementedError

    @abstractmethod
    def get_all_bookshelfs(self) -> List[Bookshelf]:
        raise NotImplementedError

    @abstractmethod
    def insert_bookshelf(self, category: Category) -> int:
        raise NotImplementedError

    @abstractmethod
    def update_bookshelf(self, id: int, category: Category):
        raise NotImplementedError

    @abstractmethod
    def delete_bookshelf(self, id: int):
        raise NotImplementedError

    @abstractmethod
    def bookshelf_exists(self, id: int) -> bool:
        raise NotImplementedError
