from abc import ABCMeta, abstractmethod
from typing import List

from api.models.book import Book


class IBookBookshelfConnector(metaclass=ABCMeta):

    @abstractmethod
    def set_book_bookshelf_table_name(self, table_name: str):
        raise NotImplementedError

    @abstractmethod
    def create_book_bookshelf_table(self):
        raise NotImplementedError

    @abstractmethod
    def book_bookshelf_table_exists(self) -> bool:
        raise NotImplementedError

    @abstractmethod
    def drop_book_bookshelf_table(self):
        raise NotImplementedError

    @abstractmethod
    def truncate_book_bookshelf_table(self):
        raise NotImplementedError

    @abstractmethod
    def get_book_bookshelf(self, book_id: int, bookshelf_id: int):
        raise NotImplementedError

    @abstractmethod
    def get_all_books_of_bookshelf(self, bookshelf_id: int) -> List[Book]:
        raise NotImplementedError

    @abstractmethod
    def add_book_to_bookshelf(self, bookshelf_id: int, book_id: int):
        raise NotImplementedError

    @abstractmethod
    def remove_book_from_bookshelf(self, bookshelf_id: int, book_id: int):
        raise NotImplementedError

    @abstractmethod
    def book_exists_in_bookshelf(self, bookshelf_id: int, book_id: int) -> bool:
        raise NotImplementedError

    @abstractmethod
    def bookshelf_and_book_exists(self, bookshelf_id: int, book_id: int) -> bool:
        raise NotImplementedError

    @abstractmethod
    def book_in_bookshelfs(self, book_id: int) -> bool:
        raise NotImplementedError

    @abstractmethod
    def get_bookshelf_id_from_book(self, book_id: int) -> int:
        raise NotImplementedError
