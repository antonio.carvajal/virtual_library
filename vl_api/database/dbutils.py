from database import dbconnectors
from properties.properties import DB_INSTANCE


def get_db():
    """
    Get the database connector instance.
    """
    try:
        db_instance = getattr(dbconnectors, DB_INSTANCE)
        pontaxians_db = db_instance()
        pontaxians_db.connect()
        yield pontaxians_db
    except Exception as e:
        raise e
    finally:
        pontaxians_db.disconnect()
