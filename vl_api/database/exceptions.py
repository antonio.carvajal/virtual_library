class CustomIntegrityError(Exception):
    def __init__(self, message: str):
        super().__init__()
        self.message = message


class DatabaseError(Exception):
    def __init__(self, message: str) -> None:
        super().__init__()
        self.message = message


class ConnectionError(Exception):
    def __init__(self, message: str) -> None:
        super().__init__()
        self.message = message


class ElementExistsError(Exception):
    def __init__(self, message: str) -> None:
        super().__init__()
        self.message = message


class UpdateError(Exception):
    def __init__(self, message: str) -> None:
        super().__init__()
        self.message = message


class DeleteError(Exception):
    def __init__(self, message: str) -> None:
        super().__init__()
        self.message = message


class NotFoundError(Exception):
    def __init__(self, message: str) -> None:
        super().__init__()
        self.message = message
