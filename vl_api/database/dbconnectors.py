from database.mysqlconnector import (AutorMySQLConnector, EditorialMySQLConnector, BookMySQLConnector,
                                     BookshelfMySQLConnector, BookBookshelfMySQLConnector)


class MySQLConnector(AutorMySQLConnector, EditorialMySQLConnector, BookMySQLConnector,
                     BookshelfMySQLConnector, BookBookshelfMySQLConnector):
    def __init__(self) -> None:
        super().__init__()
