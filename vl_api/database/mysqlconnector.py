import logging
import time
from typing import List

from mysql.connector import MySQLConnection, errors
from properties.properties import DB_HOSTNAME, DB_PORT, LIBRARY_DB, DB_PWD,\
                                  DB_USER, AUTOR_TABLENAME, EDITORIAL_TABLENAME,\
                                  BOOK_TABLENAME, BOOKSHELF_TABLENAME, BOOK_BOOKSHELF_TABLENAME,\
                                  RETRY_DELAY
from database.exceptions import DatabaseError, ConnectionError, ElementExistsError, UpdateError,\
                                DeleteError, NotFoundError
from database.idbconnectors.ibasedbconnector import IBaseDBConnector
from database.idbconnectors.iautorconnector import IAutorConnector
from database.idbconnectors.ibookconnector import IBookConnector
from database.idbconnectors.ibookshelfconnector import IBookshelfConnector
from database.idbconnectors.ieditorialconnector import IEditorialConnector
from database.idbconnectors.ibook_bookshelf_connector import IBookBookshelfConnector
from api.models.autor import Autor
from api.models.editorial import Editorial
from api.models.book import Book, Category
from api.models.bookshelf import Bookshelf


logger = logging.getLogger("vl_api")


class BaseMySQLConnector(IBaseDBConnector):

    def __init__(self):
        self.mysql_db = None
        self.cursor = None

    def connect(self) -> None:
        """
        Initialize the connection to the database
        """
        if self.is_up():
            logger.debug("DB Is already connected")
        else:
            try:
                self.mysql_db = MySQLConnection(host=DB_HOSTNAME,
                                                port=DB_PORT,
                                                user=DB_USER,
                                                passwd=DB_PWD,
                                                database=LIBRARY_DB,
                                                autocommit=True)
                self.cursor = self.mysql_db.cursor()
                logger.debug("DB connected")
            except Exception as e:
                logger.error("Cannot connect to database: " + repr(e))
                logger.warning(f"Re-trying in {RETRY_DELAY} seconds")
                time.sleep(RETRY_DELAY)
                self.connect()
                #raise ConnectionError("Cannot connect to database: " + repr(e))

    def disconnect(self) -> None:
        """
        Disconecct from the database
        """
        if self.mysql_db:
            if self.cursor:
                self.cursor.close()
            self.mysql_db.close()
        logger.debug("Disconnected from DB")

    def _run_query(self, query, _args=None) -> None:
        """
        Run a query on the database

        :param query: The query to run
        :param _args: The arguments to pass to the query
        """
        if _args:
            self.cursor.execute(query, _args)
        else:
            self.cursor.execute(query)

    def is_up(self) -> bool:
        """
        Check if the database is up

        :return: True if the database is up, False otherwise
        """
        if not self.mysql_db:
            return False
        return self.mysql_db.is_connected()

    def get_info_blob(self, blob_value) -> str:
        """
        Get the value of a blob

        :param blob_value: The value of the blob
        :return: The value of the blob as a string
        """
        return str(blob_value)


class AutorMySQLConnector(IAutorConnector, BaseMySQLConnector):

    def __init__(self) -> None:
        super().__init__()
        self.set_autor_table_name(AUTOR_TABLENAME)

    def set_autor_table_name(self, table_name: str):
        self.autor_table_name = table_name

    def create_autor_table(self):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        if self.autor_table_exists():
            logger.info("Table '" + self.autor_table_name + "' already exists")
        else:
            query = "CREATE TABLE " + self.autor_table_name + " (" \
                        "autor_id int(11) AUTO_INCREMENT NOT NULL," \
                        "name varchar(128) UNIQUE NOT NULL," \
                        "updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP," \
                    "PRIMARY KEY (`autor_id`)" \
                    ");"

            try:
                self._run_query(query)
                logger.debug("Table '" + self.autor_table_name + "' created")
            except Exception as e:
                logger.error(f"Cannot create table '{self.autor_table_name}': {repr(e)}")
                raise DatabaseError(f"Cannot create table '{self.autor_table_name}': {repr(e)}")

    def autor_table_exists(self) -> bool:

        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = "SELECT COUNT(*) FROM information_schema.tables " \
                "WHERE table_name='" + self.autor_table_name + "';"

        try:
            self._run_query(query)
        except Exception as e:
            logger.error(f"Cannot check if table '{self.autor_table_name}' exists: {repr(e)}")
            raise DatabaseError(f"Cannot check if table '{self.autor_table_name}' exists: {repr(e)}")

        return self.cursor.fetchone()[0] > 0

    def drop_autor_table(self):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = "DROP TABLE " + self.autor_table_name + ";"

        try:
            self._run_query(query)
        except Exception as e:
            logger.error(f"Cannot drop table '{self.autor_table_name}': {repr(e)}")
            raise DatabaseError(f"Cannot drop table '{self.autor_table_name}': {repr(e)}")

        logger.debug("Table '" + self.autor_table_name + "' dropped")

    def truncate_autor_table(self):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = "TRUNCATE TABLE " + self.autor_table_name + ";"

        try:
            self._run_query(query)
        except Exception as e:
            logger.error(f"Cannot truncate table '{self.autor_table_name}': {repr(e)}")
            raise DatabaseError(f"Cannot truncate table '{self.autor_table_name}': {repr(e)}")

        logger.debug("Table '" + self.autor_table_name + "' truncated")

    def insert_autor(self, name: str) -> int:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        if self.autor_exists(name):
            logger.info(f"Autor '{name}' already exists")
            raise ElementExistsError(f"Autor '{name}' already exists")

        query = f"INSERT INTO {self.autor_table_name} (name) VALUES (%s);"
        args: List = [name]

        try:
            self._run_query(query, args)
        except errors.IntegrityError:
            logger.info(f"Autor '{name}' already exists")
            raise DatabaseError(f"Autor '{name}' already exists")

        logger.debug(f"Autor '{name}' inserted")
        return self.cursor.lastrowid

    def update_autor(self, name: str, new_name: str):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"UPDATE {self.autor_table_name} SET name=%s WHERE name=%s;"
        args: List = [new_name, name]

        if not self.autor_exists(name):
            logger.info(f"Autor '{name}' does not exist")
            raise NotFoundError(f"Autor '{name}' does not exist")

        if self.autor_exists(new_name):
            logger.info(f"Autor '{new_name}' already exists")
            raise ElementExistsError(f"Autor '{new_name}' already exists")

        try:
            self._run_query(query, args)
        except Exception as e:
            logger.info(f"Cannot update autor '{name}' to '{new_name}': {repr(e)}")
            raise UpdateError(f"Cannot update autor '{name}' to '{new_name}': {repr(e)}")

        logger.debug("Row updated in DB. Table: '" + str(self.autor_table_name) + "'")

    def delete_autor(self, name: str):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        if not self.autor_exists(name):
            logger.info(f"Autor '{name}' does not exist")
            raise NotFoundError(f"Autor '{name}' does not exist")

        book_con = BookMySQLConnector()
        book_con.connect()

        if book_con.autor_have_book(name):
            logger.info(f"Autor '{name}' have books")
            raise ElementExistsError(f"Autor '{name}' have books")

        args = [name]
        query = f"DELETE FROM {self.autor_table_name} WHERE name=%s;"

        try:
            self._run_query(query, args)
        except Exception as e:
            logger.info(f"Cannot delete autor '{name}': {repr(e)}")
            raise DeleteError(f"Cannot delete autor '{name}': {repr(e)}")

        logger.debug(f"Autor '{name}' deleted")

    def autor_exists(self, name: str) -> bool:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT * FROM {self.autor_table_name} WHERE name=%s;"
        args: List = [name]

        try:
            self._run_query(query, args)
        except Exception as e:
            logger.error(f"Cannot check if autor '{name}' exists: {repr(e)}")
            raise DatabaseError(f"Cannot check if autor '{name}' exists: {repr(e)}")

        return True if self.cursor.fetchone() else False

    def get_autor(self, name: str) -> Autor:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT * FROM {self.autor_table_name} WHERE name=%s;"
        args: List = [name]

        try:
            self._run_query(query, args)
        except Exception as e:
            logger.error(f"Cannot get autor '{name}': {repr(e)}")
            raise DatabaseError(f"Cannot get autor '{name}': {repr(e)}")

        row = self.cursor.fetchone()
        autor = Autor(name=row[1]) if row else None

        return autor

    def get_autor_id(self, name: str) -> int:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT autor_id FROM {self.autor_table_name} WHERE name=%s;"
        args: List = [name]

        try:
            self._run_query(query, args)
        except Exception as e:
            logger.error(f"Cannot get autor '{name}': {repr(e)}")
            raise DatabaseError(f"Cannot get autor '{name}': {repr(e)}")

        row = self.cursor.fetchone()
        autor_id = row[0] if row else None

        return autor_id

    def get_all_autors(self) -> List[Autor]:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT * FROM {self.autor_table_name};"

        try:
            self._run_query(query)
        except Exception as e:
            logger.error(f"Cannot get all autors: {repr(e)}")
            raise DatabaseError(f"Cannot get all autors: {repr(e)}")

        rows = self.cursor.fetchall()
        autors = [Autor(name=row[1]) for row in rows] if rows else []
        return autors


class EditorialMySQLConnector(IEditorialConnector, BaseMySQLConnector):

    def __init__(self) -> None:
        super().__init__()
        self.set_editorial_table_name(EDITORIAL_TABLENAME)

    def set_editorial_table_name(self, table_name: str):
        self.editorial_table_name = table_name

    def create_editorial_table(self):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        if self.editorial_table_exists():
            logger.info(f"Table '{self.editorial_table_name}' already exists")
        else:
            query = "CREATE TABLE " + self.editorial_table_name + " (" \
                        "editorial_id INT(11) AUTO_INCREMENT NOT NULL," \
                        "name VARCHAR(128) UNIQUE NOT NULL," \
                        "address VARCHAR(128) NOT NULL," \
                        "phone VARCHAR(128)," \
                        "updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP," \
                    "CONSTRAINT "+self.editorial_table_name+"_pk PRIMARY KEY (editorial_id)" \
                    ");"

            try:
                self._run_query(query)
            except Exception as e:
                logger.error(f"Cannot create table '{self.editorial_table_name}': {repr(e)}")
                raise DatabaseError(f"Cannot create table '{self.editorial_table_name}': {repr(e)}")

            logger.debug(f"Table '{self.editorial_table_name}' created")

    def drop_editorial_table(self):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"DROP TABLE {self.editorial_table_name};"

        try:
            self._run_query(query)
        except Exception as e:
            logger.error(f"Cannot drop table '{self.editorial_table_name}': {repr(e)}")
            raise DatabaseError(f"Cannot drop table '{self.editorial_table_name}': {repr(e)}")

        logger.debug("Table '" + self.editorial_table_name + "' dropped")

    def get_all_editorials(self) -> List[Editorial]:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT * FROM {self.editorial_table_name};"

        try:
            self._run_query(query)
        except Exception as e:
            logger.error(f"Cannot get all editorials: {repr(e)}")
            raise DatabaseError(f"Cannot get all editorials: {repr(e)}")

        rows = self.cursor.fetchall()
        editorials = [Editorial(name=row[1], address=row[2], phone=row[3]) for row in rows] if rows else []
        return editorials

    def insert_editorial(self, editorial: Editorial):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        if self.editorial_exists(editorial.name):
            logger.info(f"Editorial '{editorial.name}' already exists")
            raise ElementExistsError(f"Editorial '{editorial.name}' already exists")

        query = f"INSERT INTO {self.editorial_table_name} (name, address, phone) VALUES (%s, %s, %s);"
        args: List = [editorial.name, editorial.address, editorial.phone]

        try:
            self._run_query(query, args)
        except Exception as e:
            logger.error(f"Cannot insert editorial '{editorial.name}': {repr(e)}")
            raise DatabaseError(f"Cannot insert editorial '{editorial.name}': {repr(e)}")

        logger.debug(f"Editorial '{editorial.name}' inserted")

    def delete_editorial(self, name: str):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        if not self.editorial_exists(name):
            logger.error(f"Editorial '{name}' does not exist")
            raise NotFoundError(f"Editorial '{name}' does not exist")

        book_con = BookMySQLConnector()
        book_con.connect()

        if book_con.editorial_have_book(name):
            logger.error(f"Cant delete editorial '{name}': has books")
            raise ElementExistsError(f"Cant delete Editorial '{name}': has books")

        book_con.disconnect()
        query = f"DELETE FROM {self.editorial_table_name} WHERE name=%s;"
        args: List = [name]

        try:
            self._run_query(query, args)
        except Exception as e:
            logger.error(f"Cannot delete editorial '{name}': {repr(e)}")
            raise DeleteError(f"Cannot delete editorial '{name}': {repr(e)}")

        logger.debug(f"Editorial '{name}' deleted")

    def update_editorial(self, name: str, editorial: Editorial):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        if not self.editorial_exists(name):
            logger.info(f"Editorial '{name}' does not exist")
            raise NotFoundError(f"Editorial '{name}' does not exist")

        if self.editorial_exists(editorial.name):
            logger.error(f"Editorial '{editorial.name}' already exists")
            raise ElementExistsError(f"Editorial '{editorial.name}' already exists")

        query = f"UPDATE {self.editorial_table_name} SET name=%s, address=%s, phone=%s WHERE name=%s;"
        args: List = [editorial.name, editorial.address, editorial.phone, name]

        try:
            self._run_query(query, args)
        except Exception as e:
            logger.error(f"Cannot update editorial '{name}': {repr(e)}")
            raise UpdateError(f"Cannot update editorial '{name}': {repr(e)}")

        logger.debug(f"Editorial '{name}' updated")

    def editorial_exists(self, name: str) -> bool:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT * FROM {self.editorial_table_name} WHERE name=%s;"
        args: List = [name]

        try:
            self._run_query(query, args)
        except Exception as e:
            logger.error(f"Cannot check if editorial '{name}' exists: {repr(e)}")
            raise DatabaseError(f"Cannot check if editorial '{name}' exists: {repr(e)}")

        return True if self.cursor.fetchone() else False

    def get_editorial(self, name: str) -> Editorial:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT * FROM {self.editorial_table_name} WHERE name=%s;"
        args: List = [name]

        try:
            self._run_query(query, args)
        except Exception as e:
            logger.error(f"Cannot get editorial '{name}': {repr(e)}")
            raise DatabaseError(f"Cannot get editorial '{name}': {repr(e)}")

        row = self.cursor.fetchone()
        editorial: Editorial = Editorial(name=row[1], address=row[2], phone=row[3]) if row else None
        return editorial

    def get_editorial_id(self, name: str) -> int:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT editorial_id FROM {self.editorial_table_name} WHERE name=%s;"
        args: List = [name]

        try:
            self._run_query(query, args)
        except Exception as e:
            logger.error(f"Cannot get editorial '{name}': {repr(e)}")
            raise DatabaseError(f"Cannot get editorial '{name}': {repr(e)}")

        row = self.cursor.fetchone()
        return row[0] if row else None

    def get_editorial_by_id(self, id: int) -> Editorial:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT * FROM {self.editorial_table_name} WHERE editorial_id=%s;"
        args: List = [id]

        try:
            self._run_query(query, args)
        except Exception as e:
            logger.error(f"Cannot get editorial '{id}': {repr(e)}")
            raise DatabaseError(f"Cannot get editorial '{id}': {repr(e)}")

        row = self.cursor.fetchone()
        editorial: Editorial = Editorial(name=row[1], address=row[2], phone=row[3]) if row else None
        return editorial

    def editorial_table_exists(self):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT COUNT(*) FROM information_schema.tables \
                  WHERE table_name='{self.editorial_table_name}';"

        try:
            self._run_query(query)
        except Exception as e:
            logger.error(f"Cannot check if editorial table exists: {repr(e)}")
            raise DatabaseError(f"Cannot check if editorial table exists: {repr(e)}")

        return self.cursor.fetchone()[0] > 0

    def truncate_editorial_table(self):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"TRUNCATE TABLE {self.editorial_table_name};"

        try:
            self._run_query(query)
        except Exception as e:
            logger.error(f"Cannot truncate editorial table: {repr(e)}")
            raise DatabaseError(f"Cannot truncate editorial table: {repr(e)}")

        logger.debug("Editorial table truncated")


class BookMySQLConnector(IBookConnector, BaseMySQLConnector):

    def __init__(self) -> None:
        super().__init__()
        self.set_book_table_name(BOOK_TABLENAME)

    def set_book_table_name(self, table_name: str):
        self.book_table_name = table_name

    def create_book_table(self):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        if self.book_table_exists():
            logger.info(f"Table '{self.book_table_name}' already exists")
        else:
            query = "CREATE TABLE " + self.book_table_name + " ("\
                        "book_id INT(11) AUTO_INCREMENT NOT NULL, "\
                        "title varchar(128) NOT NULL, "\
                        "synopsis varchar(255), "\
                        "category varchar(128) NOT NULL, "\
                        "autor INT(11) NOT NULL, "\
                        "editorial INT(11) NOT NULL, "\
                        "copies INT(11) NOT NULL DEFAULT 1, "\
                        "created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, "\
                        "updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, "\
                    "PRIMARY KEY (`book_id`), "\
                    "FOREIGN KEY fk_book_autor (`autor`) REFERENCES `{autor_tablename}` (autor_id), "\
                    "FOREIGN KEY fk_book_editorial (`editorial`) REFERENCES `{editorial_tablename}` (editorial_id)"\
                    ");".format(autor_tablename=AUTOR_TABLENAME, editorial_tablename=EDITORIAL_TABLENAME)

            try:
                self._run_query(query)
            except Exception as e:
                logger.error(f"Cannot create book table: {repr(e)}")
                raise DatabaseError(f"Cannot create book table: {repr(e)}")

            logger.debug(f"Table '{self.book_table_name}' created")

    def book_table_exists(self) -> bool:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT COUNT(*) FROM information_schema.tables WHERE table_name='{self.book_table_name}';"

        try:
            self._run_query(query)
        except Exception as e:
            logger.error(f"Cannot check if book table exists: {repr(e)}")
            raise DatabaseError(f"Cannot check if book table exists: {repr(e)}")

        return self.cursor.fetchone()[0] > 0

    def drop_book_table(self):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"DROP TABLE {self.book_table_name};"

        try:
            self._run_query(query)
        except Exception as e:
            logger.error(f"Cannot drop book table: {repr(e)}")
            raise DatabaseError(f"Cannot drop book table: {repr(e)}")

        logger.debug(f"Table '{self.book_table_name}' dropped")

    def truncate_book_table(self):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"TRUNCATE TABLE {self.book_table_name};"

        try:
            self._run_query(query)
        except Exception as e:
            logger.error(f"Cannot truncate book table: {repr(e)}")
            raise DatabaseError(f"Cannot truncate book table: {repr(e)}")

        logger.debug(f"Table '{self.book_table_name}' truncated")

    def get_all_books(self) -> List[Book]:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT B.book_id, B.title, B.synopsis, B.category, A.name, B.editorial, B.copies \
                  FROM {self.book_table_name} as B, autors as A WHERE B.autor = A.autor_id;"

        try:
            self._run_query(query)
        except Exception as e:
            logger.error(f"Cannot get all books: {repr(e)}")
            raise DatabaseError(f"Cannot get all books: {repr(e)}")

        rows = self.cursor.fetchall()
        editorial_con = EditorialMySQLConnector()
        editorial_con.connect()
        books = [Book(
            id=row[0],
            title=row[1],
            synopsis=row[2],
            category=row[3],
            autor=Autor(name=row[4]),
            editorial=editorial_con.get_editorial_by_id(row[5]),
            copies=row[6]) for row in rows] if rows else []
        editorial_con.disconnect()
        return books

    def get_book(self, id: int) -> Book:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT B.book_id, B.title, B.synopsis, B.category, A.name, B.editorial, B.copies \
                  FROM {self.book_table_name} as B, autors as A \
                  WHERE book_id={id} and  B.autor = A.autor_id;"

        try:
            self._run_query(query)
        except Exception as e:
            logger.error(f"Cannot get book: {repr(e)}")
            raise DatabaseError(f"Cannot get book: {repr(e)}")

        row = self.cursor.fetchone()
        editorial_con = EditorialMySQLConnector()
        editorial_con.connect()
        book = Book(
            id=row[0],
            title=row[1],
            synopsis=row[2],
            category=row[3],
            autor=Autor(name=row[4]),
            editorial=editorial_con.get_editorial_by_id(row[5]),
            copies=row[6]) if row else None
        editorial_con.disconnect()
        return book

    def get_books_by_author_name(self, name: str) -> List[Book]:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT B.book_id, B.title, B.synopsis, B.category, A.name, E.name, B.copies \
                  FROM {self.book_table_name} as B, autors as A, editorials as E \
                  WHERE B.autor = A.autor_id and B.editorial = E.editorial_id and A.name='{name}';"

        try:
            self._run_query(query)
        except Exception as e:
            logger.error(f"Cannot get books by author name: {repr(e)}")
            raise DatabaseError(f"Cannot get books by author name: {repr(e)}")

        rows = self.cursor.fetchall()
        editorial_con = EditorialMySQLConnector()
        editorial_con.connect()
        books = [Book(
            id=row[0],
            title=row[1],
            synopsis=row[2],
            category=row[3],
            autor=Autor(name=row[4]),
            editorial=editorial_con.get_editorial(row[5]),
            copies=row[6]) for row in rows] if rows else []
        editorial_con.disconnect()
        return books

    def book_exists(self, id: int) -> bool:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT COUNT(*) FROM {self.book_table_name} WHERE book_id={id};"

        try:
            self._run_query(query)
        except Exception as e:
            logger.error(f"Cannot check if book exists: {repr(e)}")
            raise DatabaseError(f"Cannot check if book exists: {repr(e)}")

        return self.cursor.fetchone()[0] > 0

    def insert_book(self, book: Book) -> int:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        if self.book_exists_by_name_editorial(book.title, book.editorial.name):
            logger.error(f"Book '{book.title}' already exists for the editorial '{book.editorial.name}'")
            raise ElementExistsError(f"Book '{book.title}' already exists for the editorial '{book.editorial.name}'")

        editorial_con = EditorialMySQLConnector()
        editorial_con.connect()

        if not editorial_con.editorial_exists(book.editorial.name):
            logger.error(f"Editorial '{book.editorial.name}' does not exist")
            raise NotFoundError(f"Editorial '{book.editorial.name}' does not exist")

        editorial_id = editorial_con.get_editorial_id(book.editorial.name)
        editorial_con.disconnect()
        autor_con = AutorMySQLConnector()
        autor_con.connect()

        if not autor_con.autor_exists(book.autor.name):
            logger.warning(f"Autor '{book.autor.name}' does not exist")
            logger.warning(f"Creating autor '{book.autor.name}'")
            autor_id = autor_con.insert_autor(book.autor.name)
        else:
            autor_id = autor_con.get_autor_id(book.autor.name)

        autor_con.disconnect()

        query = f"INSERT INTO {self.book_table_name} (title, synopsis, category, autor, editorial, copies)\
                  VALUES (%s, %s, %s, %s, %s, %s);"
        args = [book.title, book.synopsis, book.category.value, autor_id, editorial_id, book.copies]

        try:
            self._run_query(query, args)
        except Exception as e:
            logger.error(f"Cannot insert book: {repr(e)}")
            raise DatabaseError(f"Cannot insert book: {repr(e)}")

        logger.debug(f"Book '{book.title}' inserted")
        return self.cursor.lastrowid

    def book_exists_by_name_editorial(self, title: str, editorial_name: str) -> bool:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT COUNT(*) FROM {self.book_table_name} as B, editorials as E \
                  WHERE B.title=%s AND E.name=%s and B.editorial = E.editorial_id;"
        args = [title, editorial_name]

        try:
            self._run_query(query, args)
        except Exception as e:
            logger.error(f"Cannot check if book exists: {repr(e)}")
            raise DatabaseError(f"Cannot check if book exists: {repr(e)}")

        return self.cursor.fetchone()[0] > 0

    def editorial_have_book(self, editorial_name: str) -> bool:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT COUNT(*) FROM {self.book_table_name} as B, editorials as E \
                  WHERE E.name=%s and B.editorial = E.editorial_id;"
        args = [editorial_name]

        try:
            self._run_query(query, args)
        except Exception as e:
            logger.error(f"Cannot check if editorial have book: {repr(e)}")
            raise DatabaseError(f"Cannot check if editorial have book: {repr(e)}")

        return self.cursor.fetchone()[0] > 0

    def autor_have_book(self, autor_name: str) -> bool:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT COUNT(*) FROM {self.book_table_name} as B, autors as A \
                  WHERE A.name=%s and B.autor = A.autor_id;"
        args = [autor_name]

        try:
            self._run_query(query, args)
        except Exception as e:
            logger.error(f"Cannot check if autor have book: {repr(e)}")
            raise DatabaseError(f"Cannot check if autor have book: {repr(e)}")

        return self.cursor.fetchone()[0] > 0

    def delete_book(self, book_id: int):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        if not self.book_exists(book_id):
            logger.error(f"Book with id {book_id} does not exist")
            raise NotFoundError(f"Book with id {book_id} does not exist")

        bookbookshelf_con = BookBookshelfMySQLConnector()
        bookbookshelf_con.connect()

        if bookbookshelf_con.book_in_bookshelfs(book_id):
            bookshelf_id = bookbookshelf_con.get_bookshelf_id_from_book(book_id)
            bookbookshelf_con.remove_book_from_bookshelf(bookshelf_id=bookshelf_id, book_id=book_id)

        bookbookshelf_con.disconnect()

        query = f"DELETE FROM {self.book_table_name} WHERE book_id={book_id};"

        try:
            self._run_query(query)
        except Exception as e:
            logger.error(f"Cannot delete book: {repr(e)}")
            raise DatabaseError(f"Cannot delete book: {repr(e)}")

        logger.debug(f"Book '{book_id}' deleted")

    def update_book(self, book_id: int, book: Book, only_copies: bool = False):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        if not self.book_exists(book_id):
            logger.error(f"Book '{book_id}' does not exist")
            raise NotFoundError(f"Book '{book_id}' does not exist")

        if not only_copies and self.book_exists_by_name_editorial(book.title, book.editorial.name):
            logger.error(f"Book '{book.title}' already exists for the editorial '{book.editorial.name}'")
            raise ElementExistsError(f"Book '{book.title}' already exists for the editorial '{book.editorial.name}'")

        editorial_con = EditorialMySQLConnector()
        editorial_con.connect()

        if not editorial_con.editorial_exists(book.editorial.name):
            logger.error(f"Editorial '{book.editorial.name}' does not exist")
            raise NotFoundError(f"Editorial '{book.editorial.name}' does not exist")

        editorial_id = editorial_con.get_editorial_id(book.editorial.name)
        editorial_con.disconnect()
        autor_con = AutorMySQLConnector()
        autor_con.connect()

        if not autor_con.autor_exists(book.autor.name):
            logger.warning(f"Autor '{book.autor.name}' does not exist")
            logger.warning(f"Creating autor '{book.autor.name}'")
            autor_id = autor_con.insert_autor(book.autor.name)
        else:
            autor_id = autor_con.get_autor_id(book.autor.name)
        autor_con.disconnect()

        query = f"UPDATE {self.book_table_name} SET title=%s, synopsis=%s, category=%s,\
                  autor=%s, editorial=%s, copies=%s WHERE book_id=%s;"
        args = [book.title, book.synopsis, book.category.value, autor_id, editorial_id, book.copies, book_id]

        try:
            self._run_query(query, args)
        except Exception as e:
            logger.error(f"Cannot update book: {repr(e)}")
            raise DatabaseError(f"Cannot update book: {repr(e)}")

        logger.debug(f"Book '{book_id}' updated")


class BookshelfMySQLConnector(IBookshelfConnector, BaseMySQLConnector):

    def __init__(self) -> None:
        super().__init__()
        self.set_bookshelf_table_name(BOOKSHELF_TABLENAME)

    def set_bookshelf_table_name(self, table_name: str):
        self.bookshelf_table_name = table_name

    def create_bookshelf_table(self):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        if self.bookshelf_table_exists():
            logger.info("Table '" + self.bookshelf_table_name + "' already exists")
        else:
            query = "CREATE TABLE " + self.bookshelf_table_name + " (" \
                        "bookshelf_id INT(11) AUTO_INCREMENT NOT NULL, " \
                        "category varchar(128) NOT NULL, "\
                    "PRIMARY KEY (`bookshelf_id`));"

            try:
                self._run_query(query)
            except Exception as e:
                logger.error(f"Cannot create bookshelf table: {repr(e)}")
                raise DatabaseError(f"Cannot create bookshelf table: {repr(e)}")

            logger.debug(f"Table '{self.bookshelf_table_name}' created")

    def bookshelf_table_exists(self) -> bool:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT COUNT(*) FROM information_schema.tables\
                  WHERE table_name='{self.bookshelf_table_name}';"

        try:
            self._run_query(query)
        except Exception as e:
            logger.error(f"Cannot check if bookshelf table exists: {repr(e)}")
            raise DatabaseError(f"Cannot check if bookshelf table exists: {repr(e)}")

        return self.cursor.fetchone()[0] > 0

    def drop_bookshelf_table(self):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"DROP TABLE {self.bookshelf_table_name}"

        try:
            self._run_query(query)
        except Exception as e:
            logger.error(f"Cannot drop bookshelf table: {repr(e)}")
            raise DatabaseError(f"Cannot drop bookshelf table: {repr(e)}")

        logger.debug(f"Table '{self.bookshelf_table_name}' dropped")

    def truncate_bookshelf_table(self):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"TRUNCATE TABLE {self.bookshelf_table_name}"

        try:
            self._run_query(query)
        except Exception as e:
            logger.error(f"Cannot truncate bookshelf table: {repr(e)}")
            raise DatabaseError(f"Cannot truncate bookshelf table: {repr(e)}")

        logger.debug(f"Table '{self.bookshelf_table_name}' truncated")

    def get_all_bookshelfs(self) -> List[Bookshelf]:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT * FROM {self.bookshelf_table_name};"

        try:
            self._run_query(query)
        except Exception as e:
            logger.error(f"Cannot get all bookshelves: {repr(e)}")
            raise DatabaseError(f"Cannot get all bookshelves: {repr(e)}")

        rows = [row for row in self.cursor.fetchall()]
        book_bookshelf_con = BookBookshelfMySQLConnector()
        book_bookshelf_con.connect()
        bookshelfs = [Bookshelf(
                id=row[0],
                category=row[1],
                books=book_bookshelf_con.get_all_books_of_bookshelf(row[0])
            ) for row in rows] if rows else []
        book_bookshelf_con.disconnect()
        return bookshelfs

    def get_bookshelf(self, id: int) -> Bookshelf:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT * FROM {self.bookshelf_table_name} WHERE bookshelf_id=%s;"
        args = [id]

        try:
            self._run_query(query, args)
        except Exception as e:
            logger.error(f"Cannot get bookshelf: {repr(e)}")
            raise DatabaseError(f"Cannot get bookshelf: {repr(e)}")

        row = self.cursor.fetchone()

        book_bookshelf_con = BookBookshelfMySQLConnector()
        book_bookshelf_con.connect()
        bookshelf = Bookshelf(
                id=row[0],
                category=row[1],
                books=book_bookshelf_con.get_all_books_of_bookshelf(id)
            ) if row else None
        book_bookshelf_con.disconnect()
        return bookshelf

    def insert_bookshelf(self, category: Category) -> int:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"INSERT INTO {self.bookshelf_table_name} (category) VALUES (%s);"
        args = [category.value]

        try:
            self._run_query(query, args)
        except Exception as e:
            logger.error(f"Cannot insert bookshelf: {repr(e)}")
            raise DatabaseError(f"Cannot insert bookshelf: {repr(e)}")

        logger.debug(f"{category.value} category bookshelf inserted")
        return self.cursor.lastrowid

    def delete_bookshelf(self, id: int):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        if not self.bookshelf_exists(id):
            logger.error(f"Bookshelf with id {id} does not exist")
            raise NotFoundError(f"Bookshelf with id {id} does not exist")

        book_bookshelf_con = BookBookshelfMySQLConnector()
        book_bookshelf_con.connect()
        books: List[Book] = book_bookshelf_con.get_all_books_of_bookshelf(id)

        for book in books:
            logger.info(f"Deleting book {book.title} from bookshelf {id}")
            book_bookshelf_con.remove_book_from_bookshelf(id, book.id)

        book_bookshelf_con.disconnect()
        query = f"DELETE FROM {self.bookshelf_table_name} WHERE bookshelf_id={id};"

        try:
            self._run_query(query)
        except Exception as e:
            logger.error(f"Cannot delete bookshelf: {repr(e)}")
            raise DeleteError(f"Cannot delete bookshelf: {repr(e)}")

        logger.debug(f"Bookshelf '{id}' deleted")

    def update_bookshelf(self, id: int, category: Category):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"UPDATE {self.bookshelf_table_name} SET category=%s WHERE bookshelf_id=%s;"
        args = [category.value, id]

        if not self.bookshelf_exists(id):
            logger.error(f"Bookshelf '{id}' does not exist")
            raise NotFoundError(f"Bookshelf '{id}' does not exist")

        try:
            self._run_query(query, args)
        except Exception as e:
            logger.error(f"Cannot update bookshelf: {repr(e)}")
            raise DatabaseError(f"Cannot update bookshelf: {repr(e)}")

        logger.debug(f"Bookshelf '{id}' updated")

    def bookshelf_exists(self, id: int):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT COUNT(*) FROM {self.bookshelf_table_name} WHERE bookshelf_id={id};"

        try:
            self._run_query(query)
        except Exception as e:
            logger.error(f"Cannot check if bookshelf exists: {repr(e)}")
            raise DatabaseError(f"Cannot check if bookshelf exists: {repr(e)}")

        return self.cursor.fetchone()[0] > 0


class BookBookshelfMySQLConnector(IBookBookshelfConnector, BaseMySQLConnector):

    def __init__(self) -> None:
        super().__init__()
        self.set_book_bookshelf_table_name(BOOK_BOOKSHELF_TABLENAME)

    def set_book_bookshelf_table_name(self, table_name: str):
        self.book_bookshelf_tablename = table_name

    def create_book_bookshelf_table(self):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        if self.book_bookshelf_table_exists():
            logger.info("Table '" + self.book_bookshelf_tablename + "' already exists")
        else:
            query = "CREATE TABLE " + self.book_bookshelf_tablename + " ("\
                        "book_bookshelf_id INT(11) AUTO_INCREMENT NOT NULL, "\
                        "bookshelf_id INT(11) NOT NULL, "\
                        "book_id INT(11) NOT NULL, "\
                    "PRIMARY KEY (`book_bookshelf_id`), "\
                    "FOREIGN KEY fk_books_book (`book_id`) REFERENCES `{book_tablename}` (book_id),"\
                    "FOREIGN KEY fk_bookshelfs_bookshelf (`bookshelf_id`) REFERENCES `{bookshelf_tablename}` (bookshelf_id)"\
                    ");".format(book_tablename=BOOK_TABLENAME, bookshelf_tablename=BOOKSHELF_TABLENAME)

            try:
                self._run_query(query)
            except Exception as e:
                logger.error(f"Cannot create bookshelf table: {repr(e)}")
                raise DatabaseError(f"Cannot create bookshelf table: {repr(e)}")

            logger.debug(f"Table '{self.book_bookshelf_tablename}' created")

    def book_bookshelf_table_exists(self) -> bool:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT COUNT(*) FROM information_schema.tables \
                  WHERE table_name='{self.book_bookshelf_tablename}';"

        try:
            self._run_query(query)
        except Exception as e:
            logger.error(f"Cannot check if bookshelf table exists: {repr(e)}")
            raise DatabaseError(f"Cannot check if bookshelf table exists: {repr(e)}")

        return self.cursor.fetchone()[0] > 0

    def drop_book_bookshelf_table(self):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"DROP TABLE IF EXISTS {self.book_bookshelf_tablename};"

        try:
            self._run_query(query)
        except Exception as e:
            logger.error(f"Cannot drop bookshelf table: {repr(e)}")
            raise DatabaseError(f"Cannot drop bookshelf table: {repr(e)}")

        logger.debug(f"Table '{self.book_bookshelf_tablename}' dropped")

    def truncate_book_bookshelf_table(self):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"TRUNCATE TABLE {self.book_bookshelf_tablename};"

        try:
            self._run_query(query)
        except Exception as e:
            logger.error(f"Cannot truncate bookshelf table: {repr(e)}")
            raise DatabaseError(f"Cannot truncate bookshelf table: {repr(e)}")

        logger.debug(f"Table '{self.book_bookshelf_tablename}' truncated")

    def get_book_bookshelf(self, book_id: int, bookshelf_id: int):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT * FROM {self.book_bookshelf_tablename} WHERE book_id={book_id} AND bookshelf_id={bookshelf_id};"

        try:
            self._run_query(query)
        except Exception as e:
            logger.error(f"Cannot get bookshelf: {repr(e)}")
            raise DatabaseError(f"Cannot get bookshelf: {repr(e)}")

        row = self.cursor.fetchone()
        return row if row else ()

    def get_all_books_of_bookshelf(self, bookshelf_id: int) -> List[Book]:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT * FROM {self.book_bookshelf_tablename} WHERE bookshelf_id={bookshelf_id};"

        try:
            self._run_query(query)
        except Exception as e:
            logger.error(f"Cannot get bookshelf: {repr(e)}")
            raise DatabaseError(f"Cannot get bookshelf: {repr(e)}")

        rows = self.cursor.fetchall()
        books = []
        book_con = BookMySQLConnector()
        book_con.connect()

        for row in rows:
            books.append(book_con.get_book(row[2]))

        book_con.disconnect()
        return books

    def add_book_to_bookshelf(self, bookshelf_id: int, book_id: int):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        if not self.bookshelf_and_book_exists(bookshelf_id, book_id):
            logger.error(f"Bookshelf with id '{bookshelf_id}' or book with id '{book_id}' does not exist")
            raise NotFoundError(f"Bookshelf with id '{bookshelf_id}' or book with id '{book_id}' does not exist")

        if self.book_exists_in_bookshelf(bookshelf_id, book_id):
            logger.error(f"Book '{book_id}' already exists in bookshelf '{bookshelf_id}'")
            raise ElementExistsError(f"Book '{book_id}' already exists in bookshelf '{bookshelf_id}'")

        query = f"INSERT INTO {self.book_bookshelf_tablename} (bookshelf_id, book_id) VALUES (%s, %s);"
        args = [bookshelf_id, book_id]

        try:
            self._run_query(query, args)
        except Exception as e:
            logger.error(f"Cannot add book to bookshelf: {repr(e)}")
            raise DatabaseError(f"Cannot add book to bookshelf: {repr(e)}")

        logger.debug(f"Book '{book_id}' added to bookshelf '{bookshelf_id}'")

    def remove_book_from_bookshelf(self, bookshelf_id: int, book_id: int):
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        if not self.bookshelf_and_book_exists(bookshelf_id, book_id):
            logger.error(f"Book '{book_id}' or bookshelf '{bookshelf_id}' not exists")
            raise NotFoundError(f"Book '{book_id}' or bookshelf '{bookshelf_id}' not exists")

        if not self.book_exists_in_bookshelf(bookshelf_id, book_id):
            logger.error(f"Book '{book_id}' not found in bookshelf '{bookshelf_id}'")
            raise NotFoundError(f"Book '{book_id}' not found in bookshelf '{bookshelf_id}'")

        query = f"DELETE FROM {self.book_bookshelf_tablename} WHERE bookshelf_id=%s AND book_id=%s;"
        args = [bookshelf_id, book_id]

        try:
            self._run_query(query, args)
        except Exception as e:
            logger.error(f"Cannot remove book from bookshelf: {repr(e)}")
            raise DeleteError(f"Cannot remove book from bookshelf: {repr(e)}")

        logger.debug(f"Book '{book_id}' removed from bookshelf '{bookshelf_id}'")

    def book_exists_in_bookshelf(self, bookshelf_id: int, book_id: int) -> bool:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT COUNT(*) FROM {self.book_bookshelf_tablename} WHERE bookshelf_id=%s AND book_id=%s;"
        args = [bookshelf_id, book_id]

        try:
            self._run_query(query, args)
        except Exception as e:
            logger.error(f"Cannot check if book exists in bookshelf: {repr(e)}")
            raise DatabaseError(f"Cannot check if book exists in bookshelf: {repr(e)}")

        return self.cursor.fetchone()[0] > 0

    def bookshelf_and_book_exists(self, bookshelf_id: int, book_id: int) -> bool:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        booksheelf_con = BookshelfMySQLConnector()
        booksheelf_con.connect()

        if not booksheelf_con.bookshelf_exists(bookshelf_id):
            logger.error(f"Bookshelf '{bookshelf_id}' does not exist")
            return False

        booksheelf_con.disconnect()
        book_con = BookMySQLConnector()
        book_con.connect()

        if not book_con.book_exists(book_id):
            logger.error(f"Book '{book_id}' does not exist")
            return False

        book_con.disconnect()
        return True

    def book_in_bookshelfs(self, book_id: int) -> bool:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT COUNT(*) FROM {self.book_bookshelf_tablename} WHERE book_id=%s;"
        args = [book_id]

        try:
            self._run_query(query, args)
        except Exception as e:
            logger.error(f"Cannot check if book is in bookshelfs: {repr(e)}")
            raise DatabaseError(f"Cannot check if book is in bookshelfs: {repr(e)}")

        return self.cursor.fetchone()[0] > 0

    def get_bookshelf_id_from_book(self, book_id: int) -> int:
        if not self.is_up():
            logger.error("Not connected to DB")
            raise ConnectionError("Not connected to DB")

        query = f"SELECT bookshelf_id FROM {self.book_bookshelf_tablename} WHERE book_id=%s;"
        args = [book_id]

        try:
            self._run_query(query, args)
        except Exception as e:
            logger.error(f"Cannot get bookshelf id from book: {repr(e)}")
            raise DatabaseError(f"Cannot get bookshelf id from book: {repr(e)}")

        return self.cursor.fetchone()[0]
