import logging

from fastapi import FastAPI
from fastapi.exceptions import HTTPException, RequestValidationError
from starlette.exceptions import HTTPException as StarletteHTTPException
from logger.vllogger import conf_log_system
from api.endpoints import library_ep, book_ep, editorial_ep, autor_ep
from setup.setup import setup_tables
from properties.properties import OPENAPI_PREFIX
from api.exceptions import (NotFoundError, ObtainProblemError, CreateError, ExistsError,
                            UpdateError, DeleteError)
from api.handlers.handlerexceptions import (not_found_exp_handler, obtain_problem_exp_handler, http_validation_exp_handler,
                                            create_error_exp_handler, exists_exp_handler, http_starlette_exp_handler,
                                            update_error_exp_handler, http_exp_handler, delete_exp_handler)


# Configure and create logger
conf_log_system()
logger = logging.getLogger('vl_api')
app = FastAPI(title="Virtual Libarry", version="1.0.0", openapi_prefix=OPENAPI_PREFIX)

logger.debug('Adding endpoint routers...')
app.include_router(library_ep.router)
app.include_router(book_ep.router)
app.include_router(editorial_ep.router)
app.include_router(autor_ep.router)


logger.debug('Adding exception handlers...')
app.add_exception_handler(NotFoundError, not_found_exp_handler)
app.add_exception_handler(ObtainProblemError, obtain_problem_exp_handler)
app.add_exception_handler(RequestValidationError, http_validation_exp_handler)
app.add_exception_handler(CreateError, create_error_exp_handler)
app.add_exception_handler(ExistsError, exists_exp_handler)
app.add_exception_handler(StarletteHTTPException, http_starlette_exp_handler)
app.add_exception_handler(UpdateError, update_error_exp_handler)
app.add_exception_handler(HTTPException, http_exp_handler)
app.add_exception_handler(DeleteError, delete_exp_handler)


@app.on_event("startup")
async def startup():
    logger.debug("Virtual library API startup")


@app.on_event("shutdown")
async def shutdown():
    logger.debug("Shutting down virtual library API")


setup_tables()
