from pydantic import BaseModel, Field
from typing import Union, List
from properties.properties import CODE_ERRORS
from api.models.interfaces.wrapperinterface import IWrapper


class Autor(BaseModel):
    """Autor class that represent a autor"""
    name: str = Field(..., example="J. R. R. Tolkien")


class AutorWrapper(BaseModel, IWrapper):
    """Autor wrapper model"""
    code: int = CODE_ERRORS.get("ok")
    message: str = "OK"
    data: Union[Autor, List[Autor]]
