from pydantic import BaseModel, Field
from typing import List, Union

from properties.properties import CODE_ERRORS
from api.models.interfaces.wrapperinterface import IWrapper


class Editorial(BaseModel):
    """Editorial class that represent a editorial"""
    name: str = Field(..., example="Editorial 1")
    address: str = Field(..., example="Calle 1")
    phone: str = Field(..., example="123456789")


class EditorialWrapper(BaseModel, IWrapper):
    """Editorial wrapper model"""
    code: int = CODE_ERRORS.get("ok")
    message: str = "OK"
    data: Union[Editorial, List[Editorial]]
