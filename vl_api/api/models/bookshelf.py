from properties.properties import CODE_ERRORS
from typing import List, Optional, Union
from pydantic import BaseModel, Field

from api.models.interfaces.wrapperinterface import IWrapper
from api.models.book import Book, Category


class Bookshelf(BaseModel):
    """Bookshelf class that represent a bookshelf"""
    id: int = Field(..., example=1)
    category: Category = Field(..., example=Category.SCI_FI.value)
    books: Optional[List[Book]] = []


class BookshelfWrapper(BaseModel, IWrapper):
    """Bookshelf wrapper model"""
    code: int = CODE_ERRORS.get("ok")
    message: str = "OK"
    data: Union[Bookshelf, List[Bookshelf]]
