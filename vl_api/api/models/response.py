from properties.properties import CODE_ERRORS
from pydantic import BaseModel, Field

from api.models.interfaces.wrapperinterface import IWrapper


class Response(BaseModel):
    """Response class that represent a default response from some API endpoints"""
    detail: str = Field(..., example='Ok')


class ResponseWrapper(BaseModel, IWrapper):
    """Response wrapper model"""
    code: int = CODE_ERRORS.get("ok")
    message: str = "OK"
    data: Response
