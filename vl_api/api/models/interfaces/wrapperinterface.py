from abc import ABCMeta
from typing import Dict, Optional


class IWrapper(metaclass=ABCMeta):
    """Wrapper interface"""
    code: int
    message: str
    data: Dict


class IWrapperException(metaclass=ABCMeta):
    """Wrapper exception interface"""
    code: int
    message: str
    data: Optional[Dict] = {}
