from properties.properties import CODE_ERRORS
from pydantic import BaseModel, Field
from enum import Enum
from typing import Optional, List, Union

from api.models.interfaces.wrapperinterface import IWrapper
from api.models.autor import Autor
from api.models.editorial import Editorial


class Category(str, Enum):
    """Request status type model"""
    FANTASY = "Fantasy"
    ACTION = "Action"
    SCI_FI = "sci-fi"
    POETRY = "poetry"
    DRAMA = "drama"


class Book(BaseModel):
    """Book class that represent a book"""
    id: Optional[int]
    title: str = Field(..., example="The Lord of the Rings")
    synopsis: Optional[str] = Field(..., example="The Lord of the Rings is an epic high fantasy novel\
                                        written by English author and scholar J. R. R. Tolkien.")
    category: Category = Field(..., example=Category.FANTASY.value)
    autor: Autor = Field(..., example=Autor(name="J. R. R. Tolkien"))
    editorial: Editorial = Field(..., example=Editorial(name="Editorial 1", address="Calle 1", phone="123456789"))
    copies: Optional[int] = 1


class BookWrapper(BaseModel, IWrapper):
    """Bookshelf wrapper model"""
    code: int = CODE_ERRORS.get("ok")
    message: str = "OK"
    data: Union[Book, List[Book]]
