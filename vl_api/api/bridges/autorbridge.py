import logging

from fastapi import HTTPException, status
from api.exceptions import NotFoundError, ExistsError, DeleteError, ObtainProblemError,\
                           CreateError, UpdateError
from database.exceptions import UpdateError as DBUpdateError
from database.exceptions import DeleteError as DBDeleteError
from database.exceptions import NotFoundError as DBNotFoundError
from database.exceptions import DatabaseError, ElementExistsError, ConnectionError
from typing import List
from api.bridges.bridgedecorator import db_setup
from api.models.autor import Autor
from api.models.book import Book


logger = logging.getLogger('vl_api')


@db_setup
def generate_autor(name: str, db_con=None) -> Autor:
    try:
        autor: Autor = Autor(name=name)
        db_con.insert_autor(name=name)
    except ElementExistsError as ee:
        logger.error(f"Error creating the autor: {ee.message}")
        raise ExistsError(message=f"Autor {name} already exists")
    except (ConnectionError, DatabaseError) as e:
        logger.error(f"Error creating the autor: {e.message}")
        raise CreateError(message=f"Error creating the autor: {e.message}")
    except Exception as e:
        logger.error(f"Exception creating autor: {repr(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Exception creating autor: {repr(e)}")

    return autor


@db_setup
def modify_autor(autor_name: str, autor_info: Autor, db_con=None):
    try:
        db_con.update_autor(autor_name, autor_info.name)
    except ElementExistsError as ee:
        logger.error(f"Error updating the autor: {ee.message}")
        raise ExistsError(message=f"Autor {autor_info.name} already exists")
    except DBNotFoundError as dfe:
        logger.error(f"Autor not found: {dfe.message}")
        raise NotFoundError(message=f"Autor {autor_name} not found")
    except (ConnectionError, DatabaseError, DBUpdateError) as e:
        logger.error(f"Error modifying the autor: {e.message}")
        raise UpdateError(message=f"Error modifying the autor: {e.message}")
    except Exception as e:
        logger.error(f"Exception modifying autor: {repr(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Exception modifying autor: {repr(e)}")


@db_setup
def delete_autor(name: str, db_con=None):
    try:
        db_con.delete_autor(name)
    except ElementExistsError as ee:
        logger.error(f"Error deletting the autor: {ee.message}")
        raise ExistsError(message=f"Autor {name} has books")
    except DBNotFoundError as dfe:
        logger.error(f"Error deletting the autor: {dfe.message}")
        raise NotFoundError(message=f"Autor {name} not found")
    except (ConnectionError, DatabaseError, DBDeleteError) as e:
        logger.error(f"Error deleting the autor: {e.message}")
        raise DeleteError(message=f"Error deleting the autor: {e.message}")
    except Exception as e:
        logger.error(f"Exception deleting autor: {repr(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Exception deleting autor: {repr(e)}")


@db_setup
def get_autor(name: str, db_con=None) -> Autor:
    try:
        autor: Autor = db_con.get_autor(name)
    except (ConnectionError, DatabaseError) as e:
        logger.error(f"Error getting the autor: {e.message}")
        raise ObtainProblemError(message=f"Error getting the autor: {e.message}")
    except Exception as e:
        logger.error(f"Exception getting autor: {repr(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Exception getting autor: {repr(e)}")

    if not autor:
        raise NotFoundError(f"Autor {name} not found")

    return autor


@db_setup
def get_autor_books(name: str, db_con=None) -> List[Book]:
    try:
        autor_books: List[Book] = db_con.get_books_by_author_name(name)
    except (ConnectionError, DatabaseError) as e:
        logger.error(f"Error getting the autor books: {e.message}")
        raise ObtainProblemError(message=f"Error getting the autor books: {e.message}")
    except Exception as e:
        logger.error(f"Exception getting autor books: {repr(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Exception getting autor books: {repr(e)}")

    if not autor_books:
        raise NotFoundError(f"The author: {name} has no books in this library yet")

    return autor_books


@db_setup
def get_autors(db_con=None) -> List[Autor]:
    try:
        autors: List[Autor] = db_con.get_all_autors()
    except (ConnectionError, DatabaseError) as e:
        logger.error(f"Error getting the autors: {e.message}")
        raise ObtainProblemError(message=f"Error getting the autors: {e.message}")
    except Exception as e:
        logger.error(f"Exception getting autors: {repr(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Exception getting autors: {repr(e)}")

    return autors
