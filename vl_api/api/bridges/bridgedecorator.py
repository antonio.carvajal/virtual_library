from properties.properties import DB_INSTANCE
from database import dbconnectors


def db_setup(call_funct):
    def decorator_funct(*args, **kwargs):
        # Pre decorator
        db_instance = getattr(dbconnectors, DB_INSTANCE)
        db_con = db_instance()
        db_con.connect()
        result = call_funct(db_con=db_con, *args, **kwargs)
        # Post decorator
        db_con.disconnect()
        return result
    return decorator_funct
