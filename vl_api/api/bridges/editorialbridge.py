import logging

from fastapi import HTTPException, status
from api.exceptions import NotFoundError, ExistsError, DeleteError, ObtainProblemError,\
                           CreateError, UpdateError
from database.exceptions import UpdateError as DBUpdateError
from database.exceptions import DeleteError as DBDeleteError
from database.exceptions import NotFoundError as DBNotFoundError
from database.exceptions import DatabaseError, ElementExistsError, ConnectionError
from typing import List
from api.bridges.bridgedecorator import db_setup
from api.models.editorial import Editorial


logger = logging.getLogger('vl_api')


@db_setup
def generate_editorial(editorial: Editorial, db_con=None) -> Editorial:
    try:
        editorial: Editorial = editorial
        db_con.insert_editorial(editorial)
    except ElementExistsError as ee:
        logger.error(f"Error creating the editorial: {ee.message}")
        raise ExistsError(message=f"Editorial {editorial.name} already exists")
    except (ConnectionError, DatabaseError) as e:
        logger.error(f"Error creating the editorial: {e.message}")
        raise CreateError(message=f"Error creating the editorial: {e.message}")
    except Exception as e:
        logger.error(f"Exception creating editorial: {repr(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Exception creating editorial: {repr(e)}")

    return editorial


@db_setup
def modify_editorial(name: str, editorial: Editorial, db_con=None):
    try:
        db_con.update_editorial(name, editorial)
    except ElementExistsError as ee:
        logger.error(f"Error updating the editorial: {ee.message}")
        raise ExistsError(message=f"Editorial {editorial.name} already exists")
    except DBNotFoundError as dfe:
        logger.error(f"Editorial not found: {dfe.message}")
        raise NotFoundError(message=f"Editorial {name} not found")
    except (ConnectionError, DatabaseError, DBUpdateError) as e:
        logger.error(f"Error modifying the editorial {name}: {e.message}")
        raise UpdateError(message=f"Error modifying the editorial: {e.message}")
    except Exception as e:
        logger.error(f"Exception modifying editorial {name}: {repr(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Exception modifying editorial: {repr(e)}")


@db_setup
def delete_editorial(name: str, db_con=None):
    try:
        db_con.delete_editorial(name)
    except ElementExistsError as ee:
        logger.error(f"Error deleting the editorial: {ee.message}")
        raise ExistsError(message=ee.message)
    except DBNotFoundError as dfe:
        logger.error(f"Error deleting the editorial: {dfe.message}")
        raise NotFoundError(message=f"Editorial {name} not found")
    except (ConnectionError, DatabaseError, DBDeleteError) as e:
        logger.error(f"Error deleting the editorial {name}: {e.message}")
        raise DeleteError(message=f"Error deleting the editorial: {e.message}")
    except Exception as e:
        logger.error(f"Exception deleting editorial {name}: {repr(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Exception deleting editorial: {repr(e)}")


@db_setup
def get_editorial(name: str, db_con=None) -> Editorial:
    try:
        editorial: Editorial = db_con.get_editorial(name)
    except (ConnectionError, DatabaseError) as e:
        logger.error(f"Error getting the editorial: {e.message}")
        raise ObtainProblemError(message=f"Error getting the editorial: {e.message}")
    except Exception as e:
        logger.error(f"Exception getting editorial: {repr(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Exception getting editorial: {repr(e)}")

    if not editorial:
        raise NotFoundError(f"Editorial {name} not found")

    return editorial


@db_setup
def get_editorials(db_con=None) -> List[Editorial]:
    try:
        editorials: List[Editorial] = db_con.get_all_editorials()
    except (ConnectionError, DatabaseError) as e:
        logger.error(f"Error getting the editorials: {e.message}")
        raise ObtainProblemError(message=f"Error getting the editorials: {e.message}")
    except Exception as e:
        logger.error(f"Exception getting editorials: {repr(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Exception getting editorials: {repr(e)}")

    return editorials
