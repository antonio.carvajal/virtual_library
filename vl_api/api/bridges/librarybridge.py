import logging

from fastapi import HTTPException, status
from api.exceptions import NotFoundError, ExistsError, DeleteError, ObtainProblemError,\
                           CreateError, UpdateError
from database.exceptions import UpdateError as DBUpdateError
from database.exceptions import DeleteError as DBDeleteError
from database.exceptions import NotFoundError as DBNotFoundError
from database.exceptions import DatabaseError, ElementExistsError, ConnectionError
from typing import List
from api.bridges.bridgedecorator import db_setup
from api.models.bookshelf import Bookshelf, Category


logger = logging.getLogger('vl_api')


@db_setup
def generate_bookshelf(category: Category, db_con=None) -> Bookshelf:
    try:
        generated_id: int = db_con.insert_bookshelf(category)
        bookshelf: Bookshelf = Bookshelf(category=category, id=generated_id)
    except ElementExistsError as ee:
        logger.error(f"Error creating the bookshelf: {ee.message}")
        raise ExistsError(message=f"Bookshelf {bookshelf.title} already exists")
    except (ConnectionError, DatabaseError) as e:
        logger.error(f"Error creating the bookshelf: {e.message}")
        raise CreateError(message=f"Error creating the bookshelf: {e.message}")
    except Exception as e:
        logger.error(f"Exception creating bookshelf: {repr(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Exception creating bookshelf: {repr(e)}")

    return bookshelf


@db_setup
def add_book_to_bookshelf(bookshelf_id: int, book_id: int, db_con=None):
    try:
        db_con.add_book_to_bookshelf(bookshelf_id, book_id)
    except ElementExistsError as ee:
        logger.error(f"Error adding book with id {book_id} to bookshelf: {ee.message}")
        raise ExistsError(message=ee.message)
    except DBNotFoundError as dfe:
        logger.error(f"Error adding book with id {book_id} to bookshelf: {dfe.message}")
        raise NotFoundError(message=dfe.message)
    except (ConnectionError, DatabaseError) as e:
        logger.error(f"Error adding the book to the bookshelf: {e.message}")
        raise UpdateError(message=f"Error adding the book to the bookshelf: {e.message}")


@db_setup
def remove_book_to_bookshelf(bookshelf_id: int, book_id: int, db_con=None):
    try:
        db_con.remove_book_from_bookshelf(bookshelf_id, book_id)
    except DBNotFoundError as dfe:
        logger.error(f"Error removing the book wwith id {book_id} from the bookshelf: {dfe.message}")
        raise NotFoundError(message=dfe.message)
    except (ConnectionError, DatabaseError, DeleteError) as e:
        logger.error(f"Error removing the book to the bookshelf: {e.message}")
        raise UpdateError(message=f"Error removing the book to the bookshelf: {e.message}")


@db_setup
def modify_bookshelf(bookshelf_id: int, category: Category, db_con=None):
    try:
        db_con.update_bookshelf(bookshelf_id, category)
    except DBNotFoundError as dfe:
        logger.error(f"Error updating the bookshelf: {dfe.message}")
        raise NotFoundError(message=f"Bookshelf with id {bookshelf_id} not found")
    except (ConnectionError, DatabaseError, DBUpdateError) as e:
        logger.error(f"Error modifying the bookshelf: {e.message}")
        raise UpdateError(message=f"Error modifying the bookshelf: {e.message}")
    except Exception as e:
        logger.error(f"Exception modifying bookshelf: {repr(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Exception modifying bookshelf: {repr(e)}")


@db_setup
def delete_bookshelf(bookshelf_id: int, db_con=None):
    try:
        db_con.delete_bookshelf(bookshelf_id)
    except DBNotFoundError as dfe:
        logger.error(f"Error deleting the bookshelf: {dfe.message}")
        raise NotFoundError(message=f"Bookshelf with id {bookshelf_id} not found")
    except (ConnectionError, DatabaseError, DBDeleteError) as e:
        logger.error(f"Error deleting the bookshelf: {e.message}")
        raise DeleteError(message=f"Error deleting the bookshelf: {e.message}")
    except Exception as e:
        logger.error(f"Exception deleting bookshelf: {repr(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Exception deleting bookshelf: {repr(e)}")


@db_setup
def get_bookshelf(bookshelf_id: int, db_con=None) -> Bookshelf:
    try:
        bookshelf: Bookshelf = db_con.get_bookshelf(bookshelf_id)
    except (ConnectionError, DatabaseError) as e:
        logger.error(f"Error getting the bookshelf: {e.message}")
        raise ObtainProblemError(message=f"Error getting the bookshelf: {e.message}")
    except Exception as e:
        logger.error(f"Exception getting bookshelf: {repr(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Exception getting bookshelf: {repr(e)}")

    if not bookshelf:
        logger.error(f"Bookshelf with id {bookshelf_id} not found")
        raise NotFoundError("Bookshelf not found")

    return bookshelf


@db_setup
def get_bookshelfs(db_con=None) -> List[Bookshelf]:
    try:
        bookshelfs: List[Bookshelf] = db_con.get_all_bookshelfs()
    except (ConnectionError, DatabaseError) as e:
        logger.error(f"Error getting the bookshelfs: {e.message}")
        raise ObtainProblemError(message=f"Error getting the bookshelfs: {e.message}")
    except Exception as e:
        logger.error(f"Exception getting bookshelfs: {repr(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Exception getting bookshelfs: {repr(e)}")

    return bookshelfs
