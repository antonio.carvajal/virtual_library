import logging

from fastapi import HTTPException, status
from api.exceptions import NotFoundError, ExistsError, DeleteError, ObtainProblemError,\
                           CreateError, UpdateError
from database.exceptions import UpdateError as DBUpdateError
from database.exceptions import DeleteError as DBDeleteError
from database.exceptions import NotFoundError as DBNotFoundError
from database.exceptions import DatabaseError, ElementExistsError, ConnectionError
from typing import List
from api.bridges.bridgedecorator import db_setup
from api.models.book import Book


logger = logging.getLogger('vl_api')


@db_setup
def generate_book(book: Book, db_con=None) -> Book:
    try:
        generated_id: int = db_con.insert_book(book)
        book: Book = Book(
            id=generated_id,
            title=book.title,
            synopsis=book.synopsis,
            category=book.category,
            autor=book.autor,
            editorial=book.editorial,
            copies=book.copies,
        )
    except DBNotFoundError as nfe:
        logger.error(f"Error creating the book: {nfe.message}")
        raise NotFoundError(message=nfe.message)
    except ElementExistsError as ee:
        logger.error(f"Error creating the book: {ee.message}")
        raise ExistsError(message=f"Book {book.title} already exists")
    except (ConnectionError, DatabaseError) as e:
        logger.error(f"Error creating the book: {e.message}")
        raise CreateError(message=f"Error creating the book: {e.message}")
    except Exception as e:
        logger.error(f"Exception creating book: {repr(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Exception creating book: {repr(e)}")

    return book


@db_setup
def add_copies(book_id: int, copies: int, db_con=None):
    try:
        book = db_con.get_book(book_id)

        if not book:
            raise NotFoundError(message=f"Book {book_id} not found")

        book.copies += copies
        db_con.update_book(book_id, book, only_copies=True)
    except (ConnectionError, DatabaseError) as e:
        logger.error(f"Error adding copies to the book: {e.message}")
        raise UpdateError(message=f"Error adding copies to the book: {e.message}")


@db_setup
def remove_copies(book_id: int, copies: int, db_con=None):
    try:
        book = db_con.get_book(book_id)

        if not book:
            raise NotFoundError(message=f"Book {book_id} not found")

        book.copies -= copies

        if book.copies < 0:
            raise UpdateError(
                        message="You cannot delete more copies than the existing ones",
                        status_code=status.HTTP_400_BAD_REQUEST)

        db_con.update_book(book_id, book, only_copies=True)
    except (ConnectionError, DatabaseError) as e:
        logger.error(f"Error removing copies from the book: {e.message}")
        raise UpdateError(message=f"Error removing copies from the book: {e.message}")


@db_setup
def modify_book(book_id: int, book: Book, db_con=None):
    try:
        db_con.update_book(book_id, book)
    except ElementExistsError as ee:
        logger.error(f"Error modifying the book: {ee.message}")
        raise ExistsError(message=ee.message)
    except DBNotFoundError as dfe:
        logger.error(f"Error modifying the book: {dfe.message}")
        raise NotFoundError(message=f"Book with id {book_id} not found")
    except (ConnectionError, DatabaseError, DBUpdateError) as e:
        logger.error(f"Error modifying the book: {e.message}")
        raise UpdateError(message=f"Error modifying the book: {e.message}")
    except Exception as e:
        logger.error(f"Exception modifying book: {repr(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Exception modifying book: {repr(e)}")


@db_setup
def delete_book(book_id: int, db_con=None):
    try:
        db_con.delete_book(book_id)
    except DBNotFoundError as dfe:
        logger.error(f"Error deleting the bookshelf: {dfe.message}")
        raise NotFoundError(message=f"Book with id {book_id} not found")
    except (ConnectionError, DatabaseError, DBDeleteError) as e:
        logger.error(f"Error deleting the book: {e.message}")
        raise DeleteError(message=f"Error deleting the book: {e.message}")
    except Exception as e:
        logger.error(f"Exception deleting book: {repr(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Exception deleting book: {repr(e)}")


@db_setup
def get_book(book_id: int, db_con=None) -> Book:
    try:
        book: Book = db_con.get_book(book_id)
    except (ConnectionError, DatabaseError) as e:
        logger.error(f"Error getting the book: {e.message}")
        raise ObtainProblemError(message=f"Error getting the book: {e.message}")
    except Exception as e:
        logger.error(f"Exception getting book: {repr(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Exception getting book: {repr(e)}")

    if not book:
        raise NotFoundError(f"Book with id {book_id} not found")

    return book


@db_setup
def get_books(db_con=None) -> List[Book]:
    try:
        books: List[Book] = db_con.get_all_books()
    except (ConnectionError, DatabaseError) as e:
        logger.error(f"Error getting the books: {e.message}")
        raise ObtainProblemError(message=f"Error getting the books: {e.message}")
    except Exception as e:
        logger.error(f"Exception getting books: {repr(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Exception getting books: {repr(e)}")

    return books
