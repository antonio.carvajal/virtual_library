from typing import Any, Optional

from pydantic import BaseModel
from api.models.interfaces.wrapperinterface import IWrapperException
from fastapi import status, HTTPException


class WrapperException(BaseModel, IWrapperException):
    code: int
    message: str
    data: Optional[Any] = {}


class NotFoundError(Exception):

    def __init__(self, message: str, status_code: int = status.HTTP_404_NOT_FOUND):
        self.message = message
        self.status_code = status_code
        super().__init__(self.message)

    def __str__(self):
        return repr(self.message)


class ExistsError(Exception):

    def __init__(self, message: str, status_code: int = status.HTTP_400_BAD_REQUEST):
        self.message = message
        self.status_code = status_code
        super().__init__(self.message)

    def __str__(self):
        return repr(self.message)


class DeleteError(Exception):

    def __init__(self, message: str, status_code: int = status.HTTP_500_INTERNAL_SERVER_ERROR):
        self.message = message
        self.status_code = status_code
        super().__init__(self.message)

    def __str__(self):
        return repr(self.message)


class ObtainProblemError(Exception):

    def __init__(self, message: str, status_code: int = status.HTTP_500_INTERNAL_SERVER_ERROR):
        self.message = message
        self.status_code = status_code
        super().__init__(self.message)

    def __str__(self):
        return repr(self.message)


class CreateError(HTTPException):
    def __init__(self, message: str, status_code: int = status.HTTP_500_INTERNAL_SERVER_ERROR):
        super().__init__(status_code=status_code)
        self.message = message
        self.status_code = status_code

    def __str__(self):
        return repr(self.message)


class UpdateError(HTTPException):
    def __init__(self, message: str, status_code: int = status.HTTP_500_INTERNAL_SERVER_ERROR):
        super().__init__(status_code=status_code)
        self.message = message
        self.status_code = status_code

    def __str__(self):
        return repr(self.message)
