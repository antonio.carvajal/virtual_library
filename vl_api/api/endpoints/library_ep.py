from typing import List

from fastapi import Path, APIRouter, HTTPException, status
from fastapi import Request as FAPIRequest
from api.exceptions import NotFoundError, ExistsError, UpdateError,\
                           ObtainProblemError, CreateError, DeleteError
from api.models.bookshelf import BookshelfWrapper
from api.models.book import Category
from api.bridges import librarybridge
from api.models.response import ResponseWrapper, Response
from api.security.audit import audit_request


router = APIRouter()


@router.post("/bookshelf/", tags=["Library"], response_model=BookshelfWrapper, status_code=status.HTTP_201_CREATED)
def create_bookshelf(
        fastapi_request: FAPIRequest,
        category: Category = ...,
        ) -> BookshelfWrapper:
    audit_request(fastapi_request.client.host, "Create bookshelf")
    try:
        bookshelf: BookshelfWrapper = BookshelfWrapper(data=librarybridge.generate_bookshelf(category))
    except (ExistsError, CreateError) as e:
        raise e
    except Exception:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return bookshelf


@router.post(
        "/bookshelf/{bookshelf_id}/book/{book_id}/",
        tags=["Library"],
        response_model=ResponseWrapper,
        status_code=status.HTTP_200_OK
        )
def add_book_to_bookshelf(
        fastapi_request: FAPIRequest,
        bookshelf_id: int = Path(...),
        book_id: int = Path(...),
        ) -> ResponseWrapper:
    audit_request(fastapi_request.client.host, "Add book to bookshelf")
    try:
        librarybridge.add_book_to_bookshelf(bookshelf_id, book_id)
        response: ResponseWrapper = ResponseWrapper(data=Response(detail="Book successfully added to bookshelf"))
    except (NotFoundError, UpdateError, HTTPException, ExistsError) as e:
        raise e
    except Exception:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return response


@router.delete(
        "/bookshelf/{bookshelf_id}/book/{book_id}/",
        tags=["Library"],
        response_model=ResponseWrapper,
        status_code=status.HTTP_200_OK
        )
def remove_book_to_bookshelf(
        fastapi_request: FAPIRequest,
        bookshelf_id: int = Path(...),
        book_id: int = Path(...),
        ) -> ResponseWrapper:
    audit_request(fastapi_request.client.host, "Add book to bookshelf")
    try:
        librarybridge.remove_book_to_bookshelf(bookshelf_id, book_id)
        response: ResponseWrapper = ResponseWrapper(data=Response(detail="Book successfully added to bookshelf"))
    except (NotFoundError, DeleteError, HTTPException) as e:
        raise e
    except Exception:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return response


@router.put("/bookshelf/{bookshelf_id}/", tags=["Library"], response_model=ResponseWrapper, status_code=status.HTTP_200_OK)
def modify_bookshelf(
        fastapi_request: FAPIRequest,
        bookshelf_id: int = Path(...),
        category: Category = ...,
        ) -> ResponseWrapper:
    audit_request(fastapi_request.client.host, "Modify bookshelf")
    try:
        librarybridge.modify_bookshelf(bookshelf_id, category)
        response: ResponseWrapper = ResponseWrapper(data=Response(detail="Bookshelf successfully modified"))
    except (NotFoundError, UpdateError, HTTPException) as e:
        raise e
    except Exception:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return response


@router.delete(
        "/bookshelf/{bookshelf_id}/",
        tags=["Library"],
        response_model=ResponseWrapper,
        status_code=status.HTTP_200_OK
        )
def delete_bookshelf(
        fastapi_request: FAPIRequest,
        bookshelf_id: int = Path(...)
        ) -> ResponseWrapper:
    audit_request(fastapi_request.client.host, "Delete bookshelf")
    try:
        librarybridge.delete_bookshelf(bookshelf_id)
        response: ResponseWrapper = ResponseWrapper(data=Response(detail="Bookshelf successfully deleted"))
    except (NotFoundError, DeleteError, HTTPException) as e:
        raise e
    except Exception:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return response


@router.get("/bookshelf/{bookshelf_id}/", tags=["Library"], response_model=BookshelfWrapper, status_code=status.HTTP_200_OK)
def get_bookshelf(
        fastapi_request: FAPIRequest,
        bookshelf_id: int = Path(...)
        ) -> BookshelfWrapper:
    audit_request(fastapi_request.client.host, "Get bookshelf")
    try:
        bookshelf: BookshelfWrapper = BookshelfWrapper(data=librarybridge.get_bookshelf(bookshelf_id))
    except (NotFoundError, ObtainProblemError, HTTPException) as e:
        raise e
    except Exception:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return bookshelf


@router.get("/bookshelf/", tags=["Library"], response_model=BookshelfWrapper, status_code=status.HTTP_200_OK)
def get_bookshelfs(
        fastapi_request: FAPIRequest,
        ) -> List[BookshelfWrapper]:
    audit_request(fastapi_request.client.host, "Get bookshelfs")
    try:
        bookshelfs: BookshelfWrapper = BookshelfWrapper(data=librarybridge.get_bookshelfs())
    except (ObtainProblemError, HTTPException) as e:
        raise e
    except Exception:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return bookshelfs
