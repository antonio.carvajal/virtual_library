from fastapi import Path, APIRouter, HTTPException, status, Query
from fastapi import Request as FAPIRequest
from api.exceptions import NotFoundError, ExistsError, UpdateError,\
                           ObtainProblemError, CreateError, DeleteError
from api.models.autor import AutorWrapper, Autor
from api.models.book import BookWrapper
from api.bridges import autorbridge
from api.models.response import ResponseWrapper, Response
from api.security.audit import audit_request


router = APIRouter()


@router.post("/autor/", tags=["Author"], response_model=AutorWrapper, status_code=status.HTTP_201_CREATED)
def create_autor(
        fastapi_request: FAPIRequest,
        name: str = Query(..., example="J. R. R. Tolkien"),
        ) -> AutorWrapper:
    audit_request(fastapi_request.client.host, "Create autor")
    try:
        autor: AutorWrapper = AutorWrapper(data=autorbridge.generate_autor(name))
    except (ExistsError, CreateError) as e:
        raise e
    except Exception:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return autor


@router.put("/autor/{autor_name}/", tags=["Author"], response_model=ResponseWrapper, status_code=status.HTTP_200_OK)
def modify_autor(
        fastapi_request: FAPIRequest,
        autor_name: str = Path(...),
        autor_info: Autor = ...,
        ) -> ResponseWrapper:
    audit_request(fastapi_request.client.host, "Modify autor")
    try:
        autorbridge.modify_autor(autor_name, autor_info)
        response: ResponseWrapper = ResponseWrapper(data=Response(detail="Autor successfully modified"))
    except (ExistsError, NotFoundError, UpdateError, HTTPException) as e:
        raise e
    except Exception:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return response


@router.delete("/autor/{autor_name}/", tags=["Author"], response_model=ResponseWrapper, status_code=status.HTTP_200_OK)
def delete_autor(
        fastapi_request: FAPIRequest,
        autor_name: str = Path(...)
        ) -> ResponseWrapper:
    audit_request(fastapi_request.client.host, "Delete autor")
    try:
        autorbridge.delete_autor(autor_name)
        response: ResponseWrapper = ResponseWrapper(data=Response(detail="Autor successfully deleted"))
    except (NotFoundError, DeleteError, HTTPException, ExistsError) as e:
        raise e
    except Exception:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return response


@router.get("/autor/{autor_name}/", tags=["Author"], response_model=AutorWrapper, status_code=status.HTTP_200_OK)
def get_autor(
        fastapi_request: FAPIRequest,
        autor_name: str = Path(...)
        ) -> AutorWrapper:
    audit_request(fastapi_request.client.host, "Get autor")
    try:
        autor: AutorWrapper = AutorWrapper(data=autorbridge.get_autor(autor_name))
    except (NotFoundError, ObtainProblemError, HTTPException) as e:
        raise e
    except Exception:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return autor


@router.get("/autor/{autor_name}/books/", tags=["Author"], response_model=BookWrapper, status_code=status.HTTP_200_OK)
def get_autor_books(
        fastapi_request: FAPIRequest,
        autor_name: str = Path(...),
        ) -> BookWrapper:
    audit_request(fastapi_request.client.host, "Get autor books")
    try:
        books: BookWrapper = BookWrapper(data=autorbridge.get_autor_books(autor_name))
    except (NotFoundError, ObtainProblemError, HTTPException) as e:
        raise e
    except Exception:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return books


@router.get("/autor/", tags=["Author"], response_model=AutorWrapper, status_code=status.HTTP_200_OK)
def get_autors(
        fastapi_request: FAPIRequest,
        ) -> AutorWrapper:
    audit_request(fastapi_request.client.host, "Get autors")
    try:
        autors_wrapper: AutorWrapper = AutorWrapper(data=autorbridge.get_autors())
    except (ObtainProblemError, HTTPException) as e:
        raise e
    except Exception:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return autors_wrapper
