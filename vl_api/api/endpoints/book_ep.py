from fastapi import Path, APIRouter, HTTPException, status, Query
from fastapi import Request as FAPIRequest
from api.exceptions import NotFoundError, ExistsError, UpdateError,\
                           ObtainProblemError, CreateError, DeleteError
from api.models.book import Book, BookWrapper
from api.bridges import bookbridge
from api.models.response import ResponseWrapper, Response
from api.security.audit import audit_request


router = APIRouter()


@router.post("/book/", tags=["Book"], response_model=BookWrapper, status_code=status.HTTP_201_CREATED)
def create_book(
        fastapi_request: FAPIRequest,
        model: Book = ...,
        ) -> BookWrapper:
    audit_request(fastapi_request.client.host, "Create book")
    try:
        book: BookWrapper = BookWrapper(data=bookbridge.generate_book(model))
    except (ExistsError, CreateError, NotFoundError) as e:
        raise e
    except Exception:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return book


@router.put("/book/{book_id}/copies/add/", tags=["Book"], response_model=ResponseWrapper, status_code=status.HTTP_200_OK)
def add_copies(
        fastapi_request: FAPIRequest,
        book_id: int,
        copies: int = Query(..., gt=0, example=1),
        ) -> ResponseWrapper:
    audit_request(fastapi_request.client.host, "Add copies to book")
    try:
        bookbridge.add_copies(book_id, copies)
        response: ResponseWrapper = ResponseWrapper(data=Response(detail="Copies added successfully"))
    except (NotFoundError, UpdateError) as e:
        raise e
    except Exception:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return response


@router.put("/book/{book_id}/copies/remove/", tags=["Book"], response_model=ResponseWrapper, status_code=status.HTTP_200_OK)
def remove_copies(
        fastapi_request: FAPIRequest,
        book_id: int,
        copies: int = Query(..., gt=0, example=1),
        ) -> ResponseWrapper:
    audit_request(fastapi_request.client.host, "Remove copies from book")
    try:
        bookbridge.remove_copies(book_id, copies)
        response: ResponseWrapper = ResponseWrapper(data=Response(detail="Copies removed successfully"))
    except (NotFoundError, UpdateError) as e:
        raise e
    except Exception:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return response


@router.put("/book/{book_id}/", tags=["Book"], response_model=ResponseWrapper, status_code=status.HTTP_200_OK)
def modify_book(
        fastapi_request: FAPIRequest,
        book_id: int = Path(...),
        book: Book = ...,
        ) -> ResponseWrapper:
    audit_request(fastapi_request.client.host, "Modify book")
    try:
        bookbridge.modify_book(book_id, book)
        response: ResponseWrapper = ResponseWrapper(data=Response(detail="Book successfully modified"))
    except (NotFoundError, UpdateError, HTTPException, ExistsError) as e:
        raise e
    except Exception:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return response


@router.delete("/book/{book_id}/", tags=["Book"], response_model=ResponseWrapper, status_code=status.HTTP_200_OK)
def delete_book(
        fastapi_request: FAPIRequest,
        book_id: int = Path(...)
        ) -> ResponseWrapper:
    audit_request(fastapi_request.client.host, "Delete book")
    try:
        bookbridge.delete_book(book_id)
        response: ResponseWrapper = ResponseWrapper(data=Response(detail="Book successfully deleted"))
    except (NotFoundError, DeleteError, HTTPException, ExistsError) as e:
        raise e
    except Exception:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return response


@router.get("/book/{book_id}/", tags=["Book"], response_model=BookWrapper, status_code=status.HTTP_200_OK)
def get_book(
        fastapi_request: FAPIRequest,
        book_id: int = Path(...)
        ) -> BookWrapper:
    audit_request(fastapi_request.client.host, "Get book")
    try:
        book: BookWrapper = BookWrapper(data=bookbridge.get_book(book_id))
    except (NotFoundError, ObtainProblemError, HTTPException) as e:
        raise e
    except Exception:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return book


@router.get("/book/", tags=["Book"], response_model=BookWrapper, status_code=status.HTTP_200_OK)
def get_books(
        fastapi_request: FAPIRequest,
        ) -> BookWrapper:
    audit_request(fastapi_request.client.host, "Get books")
    try:
        books: BookWrapper = BookWrapper(data=bookbridge.get_books())
    except (ObtainProblemError, HTTPException) as e:
        raise e
    except Exception:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return books
