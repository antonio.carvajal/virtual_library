from fastapi import Path, APIRouter, HTTPException, status
from fastapi import Request as FAPIRequest
from api.exceptions import NotFoundError, ExistsError, UpdateError,\
                           ObtainProblemError, CreateError, DeleteError
from api.models.editorial import EditorialWrapper, Editorial
from api.bridges import editorialbridge
from api.models.response import ResponseWrapper, Response
from api.security.audit import audit_request


router = APIRouter()


@router.post("/editorial/", tags=["Editorial"], response_model=EditorialWrapper, status_code=status.HTTP_201_CREATED)
def create_editorial(
        fastapi_request: FAPIRequest,
        model: Editorial = ...,
        ) -> EditorialWrapper:
    audit_request(fastapi_request.client.host, "Create editorial")
    try:
        editorial: EditorialWrapper = EditorialWrapper(data=editorialbridge.generate_editorial(model))
    except (ExistsError, CreateError) as e:
        raise e
    except Exception:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return editorial


@router.put("/editorial/{editorial_name}/", tags=["Editorial"], response_model=ResponseWrapper, status_code=status.HTTP_200_OK)
def modify_editorial(
        fastapi_request: FAPIRequest,
        editorial_name: str = Path(...),
        editorial: Editorial = ...,
        ) -> ResponseWrapper:
    audit_request(fastapi_request.client.host, "Modify editorial")
    try:
        editorialbridge.modify_editorial(editorial_name, editorial)
        response: ResponseWrapper = ResponseWrapper(data=Response(detail="Editorial successfully modified"))
    except (ExistsError, NotFoundError, UpdateError, HTTPException) as e:
        raise e
    except Exception:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return response


@router.delete("/editorial/{editorial_name}/", tags=["Editorial"], response_model=ResponseWrapper, status_code=status.HTTP_200_OK)
def delete_editorial(
        fastapi_request: FAPIRequest,
        editorial_name: str = Path(...)
        ) -> ResponseWrapper:
    audit_request(fastapi_request.client.host, "Delete editorial")
    try:
        editorialbridge.delete_editorial(editorial_name)
        response: ResponseWrapper = ResponseWrapper(data=Response(detail="Editorial successfully deleted"))
    except (NotFoundError, DeleteError, HTTPException, ExistsError) as e:
        raise e
    except Exception:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return response


@router.get("/editorial/{editorial_name}/", tags=["Editorial"], response_model=EditorialWrapper, status_code=status.HTTP_200_OK)
def get_editorial(
        fastapi_request: FAPIRequest,
        editorial_name: str = Path(...)
        ) -> EditorialWrapper:
    audit_request(fastapi_request.client.host, "Get editorial")
    try:
        editorial: EditorialWrapper = EditorialWrapper(data=editorialbridge.get_editorial(editorial_name))
    except (NotFoundError, ObtainProblemError, HTTPException) as e:
        raise e
    except Exception:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return editorial


@router.get("/editorial/", tags=["Editorial"], response_model=EditorialWrapper, status_code=status.HTTP_200_OK)
def get_editorials(
        fastapi_request: FAPIRequest,
        ) -> EditorialWrapper:
    audit_request(fastapi_request.client.host, "Get editorials")
    try:
        editorials: EditorialWrapper = EditorialWrapper(data=editorialbridge.get_editorials())
    except (ObtainProblemError, HTTPException) as e:
        raise e
    except Exception:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return editorials
