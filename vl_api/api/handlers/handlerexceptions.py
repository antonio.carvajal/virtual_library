from fastapi.exceptions import HTTPException, RequestValidationError
from starlette.exceptions import HTTPException as StarletteHTTPException
from starlette.status import HTTP_422_UNPROCESSABLE_ENTITY
from properties.properties import CODE_ERRORS

from api.exceptions import NotFoundError, ObtainProblemError, CreateError, ExistsError,\
                           UpdateError, WrapperException, DeleteError
from fastapi import Request
from fastapi.responses import JSONResponse


async def create_error_exp_handler(request: Request, ce: CreateError):
    return JSONResponse(
        status_code=ce.status_code,
        content=WrapperException(code=CODE_ERRORS.get("create_code_error"), message=ce.message).dict()
    )


async def update_error_exp_handler(request: Request, ce: UpdateError):
    return JSONResponse(
        status_code=ce.status_code,
        content=WrapperException(code=CODE_ERRORS.get("update_code_error"), message=ce.message).dict()
    )


async def http_exp_handler(request: Request, hte: HTTPException):
    return JSONResponse(
        status_code=hte.status_code,
        content=WrapperException(code=CODE_ERRORS.get("http_code_error"), message=hte.detail).dict()
    )


async def not_found_exp_handler(request: Request, nfe: NotFoundError):
    return JSONResponse(
        status_code=nfe.status_code,
        content=WrapperException(code=CODE_ERRORS.get("not_found_code_error"), message=nfe.message).dict()
    )


async def obtain_problem_exp_handler(request: Request, ope: ObtainProblemError):
    return JSONResponse(
        status_code=ope.status_code,
        content=WrapperException(code=CODE_ERRORS.get("obtain_problem_code_error"), message=ope.message).dict()
    )


async def exists_exp_handler(request: Request, ee: ExistsError):
    return JSONResponse(
        status_code=ee.status_code,
        content=WrapperException(code=CODE_ERRORS.get("exists_code_error"), message=ee.message).dict()
    )


async def http_validation_exp_handler(request: Request, rve: RequestValidationError):
    return JSONResponse(
        status_code=HTTP_422_UNPROCESSABLE_ENTITY,
        content=WrapperException(
                code=CODE_ERRORS.get("validation_code_error"),
                message="Validation Error: Unprocessable entity",
                extra=rve.errors()
            ).dict()
    )


async def http_starlette_exp_handler(request: Request, sth: StarletteHTTPException):
    return JSONResponse(
        status_code=sth.status_code,
        content=WrapperException(code=CODE_ERRORS.get("http_code_error"), message=sth.detail).dict()
    )


async def delete_exp_handler(request: Request, de: DeleteError):
    return JSONResponse(
        status_code=de.status_code,
        content=WrapperException(code=CODE_ERRORS.get("delete_code_error"), message=de.message).dict()
    )
