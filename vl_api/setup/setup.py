import logging

from properties.properties import DB_INSTANCE
from database import dbconnectors


logger = logging.getLogger('vl_api')


def setup_tables() -> None:
    db_instance = getattr(dbconnectors, DB_INSTANCE)
    db_con = db_instance()
    db_con.connect()

    logger.info("Creating tables...")

    if not db_con.autor_table_exists():
        db_con.create_autor_table()

    if not db_con.editorial_table_exists():
        db_con.create_editorial_table()

    if not db_con.create_book_table():
        db_con.book_table_exists()

    if not db_con.bookshelf_table_exists():
        db_con.create_bookshelf_table()

    if not db_con.book_bookshelf_table_exists():
        db_con.create_book_bookshelf_table()

    logger.info("Tables created successfully")
    db_con.disconnect()
