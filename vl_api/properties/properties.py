import os


APP_PATH = os.getenv("APP_PATH", "./")

###################
# LOGGER PROPERTIES #
###################
LOG_CONF_PATH = os.path.join(APP_PATH, os.path.join('properties', 'log_config.yml'))

###################
# API PROPERTIES #
###################
API_HOST = "0.0.0.0"
API_PORT = 8080
OPENAPI_PREFIX = os.getenv('ROOT_PATH', '')

###################
# API CODE ERRORS #
###################
CODE_ERRORS = {
    "ok": 0,
    "not_found_code_error": 1,
    "obtain_problem_code_error": 2,
    "create_code_error": 3,
    "update_code_error": 4,
    "delete_code_error": 5,
    "exists_code_error": 6,
    "http_code_error": 7,
    "validation_code_error": 8
}

#######################
# DATABASE PROPERTIES #
#######################
DB_INSTANCE = "MySQLConnector"
RETRY_DELAY = 10
DB_HOSTNAME = "library-db"
DB_PORT = 3306
LIBRARY_DB = "library"
DB_PWD = "admin"
DB_USER = "admin"

AUTOR_TABLENAME = "autors"
EDITORIAL_TABLENAME = "editorials"
BOOK_TABLENAME = "books"
BOOKSHELF_TABLENAME = "bookshelfs"
BOOK_BOOKSHELF_TABLENAME = "books_bookshelfs"
