**--- Libería virtual ---**

### Pre-requisitos 📋
 - Para el funcionamiento de esta aplicación mediante contenedores debes tener instalado docker en su equipo (https://www.docker.com/)
 - De forma opcional se puede levantar en local, para ello debes tener instalado Python3.10 en su equipo (https://www.python.org/downloads/). También require de Docker ya que utilizaremos una base de datos dockerizada

### Comenzando 🚀
 
 - **Docker Compose**
 
  - Para arrancar la aplicación unicamente situese en el directorio raiz una vez clonado el repositorio y use el comando **docker compose up**. Opcionalmente puede añadir el flag **-b** para que la ejecución pase a un segundo plano

  - Una vez arrancado podemos acceder a la aplicación directamente ingresando en el navegador la siguiente dirección: **http://localhost:8000/library**

  - También puede hacer uso de la aplicación directamente desde la API **http://localhost:8080/docs**

 - **Kubernetes**
  - También se puede desplegar la aplicación utilizando un cluster local de k8s como kind o minikube. Para ello haremos uso de la herramienta integrada "kustomize"

    ## Crear el cluster

  Primero deberemos desplegar nuestro cluster kind utilizando el fichero "kind-config.yml" con el siguiente comando:

  ```shell
  kind create cluster --config .\kind-config.yaml
  ```

    ## Crear el directorio para almacenar los datos de la BD (PV)
  
  Deberemos crear manualmente con el siguiente comando el directorio /data en /mnt/data que posteriormente será utilizado para el PV

  ```shell
  docker exec kind-control-plane ls /mnt/data
  ```

    ## Desplegar la aplicación
  
  Si estás desplegando todo en un cluster local deberás configurar y desplegar un controlador ingress para poder reenviar los puertos. En este caso utilizaremos "Ingress NGINX", lo haremos con el siguiente comando:

  ```shell
  kubectl apply -f ./ingress-nginx/ingress-nginx.yml
  ```

  Hay que esperar a que el componente esté listo (Se puede comprobar utilizando el siguiente comando):
  
  ```shell
  kubectl wait --namespace ingress-nginx --for=condition=ready pod --selector=app.kubernetes.io/component=controller --timeout=120s
  ```

  Todos los elementos que componen la aplicación están definidos en el fichero `kustomization.yml`. Para desplegar la
  aplicación, desde el directorio `kubernetes` se ejecuta:

  ```shell
  kubectl apply -k ./
  ```

  Para desplegar el dashboard se debe usar el siguiente comando:

  ```shell
  kubectl apply -f ./k8s-dashboard/k8s-dashboard.yml
  ```

  Lo siguiente es activar el proxy para poder acceder

  ```shell
  kubectl proxy
  ```

  Crearemos un usuario para poder acceder al dashboard (warning: este usuario tendrá permisos de administrador)

  ```shell
  kubectl.exe apply -f ./k8s-dashboard/k8s-dashboard-admin-user.yml
  ```

  Por último hay que generar un token para poder acceder (este comando requiere una versión tanto del cliente como del servidor de k8s >=1.24.0)

  ```shell
  kubectl -n kubernetes-dashboard create token admin-user
  ```

  Podremos acceder a través del siguient enlace: http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/

    ## Argo CD
    
  Requiere tener instaldo CLI de Argo --> https://argo-cd.readthedocs.io/en/stable/cli_installation/

  Primero deberemos desplegar "Cert manager" para usar certificados autofirmados. Los haremos con el siguiente comando:

  ```shell
  kubectl apply --validate=false -f .\cert-manager\cert-manager.yml
  ```

  Ahora requerimos de un issuer

  ```shell
  kubectl apply -f .\cert-manager\cert-issuer.yaml
  ```
  
  A continuación vamos a desplegar Argo CD. Lo primero es crear su propio namespaces con el siguiente comando:

  ```shell
  kubectl create namespace Argo CD
  ```

  Ahora tenemos que desplegar Argo CD con el siguiente comando:

  ```shell
  kubectl apply -n argocd -f ./argo-cd/argo-cd.yml
  ```

  Posteriormente deberemos de desplegar la regla de ingress para Argo:
  Nota --> Cambie el host por su dominio o configure el fichero /hosts/ de su sistema operativo con la siguiente linea "127.0.0.1       localhost argocd.local"

  ```shell
  kubectl apply -n argocd -f .\ingress-nginx\ingress-mapping-argo.yml
  ```

  Para iniciar sesión como administrador de beremos de recuperar la contraseña que se almacena en el campo "password" en un secreto nombrado "argocd-initial-admin-secret" que se generó automaticamente (nombre de usuario admin). Usaremos el siguiente comando:

  ```shell
  argocd login <ARGOCD_SERVER>
  ```

  Es altamente recomendado cambiar la contraseña una vez estemos logeados con el siguiente comando:

  ```shell
  argocd account update-password
  ```

  Deberiamos poder acceder a la UI a través de la siguiente URL: "https://argocd.local/"

  Ahora Registre un clúster para implementar aplicaciones (opcional)
  Este paso registra las credenciales de un clúster en Argo CD y solo es necesario cuando se implementa en un clúster externo. Cuando se implementa internamente (en el mismo clúster en el que se ejecuta Argo CD), se debe usar https://kubernetes.default.svc como la dirección del servidor API K8s de la aplicación.

  Primero enumere todos los contextos de clústeres en su kubeconfig actual:

  ```shell
  kubectl config get-contexts -o name
  ```

  Elija un nombre de contexto de la lista y proporciónelo a argocd cluster add CONTEXTNAME. Por ejemplo, para el contexto kind-kind, ejecute:

  ```shell
  argocd cluster add kind-kind
  ```

 - **Local**

  - Para arrancar la aplicación desde local debemos tener arrancado previamente un contenedor de MySQL (docker) **docker run -it --name {"Nombre_de_tu_mysql"} -p 3306:3306 -e MYSQL_USER={username} -e MYSQL_PASSWORD={password} -e MYSQL_ROOT_PASSWORD={root_password} -e MYSQL_DATABASE=library mysql** o consulte el repositorio oficial de la imagen de MySQL (https://hub.docker.com/_/mysql)

  - Una vez arrancado nos dirigiremos a la ruta **vl_api**, arrancaremos nuestro entorno virtual (recomendable) e instalaremos las librerias requeridas **python3 -m pip install -r requirement.txt**. Posteriormente arrancamos la API con el comando **uvicorn main:app --host 0.0.0.0 --port 8080** (La estructura de la base de datos se construirá una vez arranque esta aplicación)

  - Por último (desde otra terminal) debemos dirigirnos a la ruta **vl_front**, instalar nuevamente las liberías en nuestro entorno virtual y arrancar la aplicación con el siguiente comando **python3 manage.py runserver --noreload 0.0.0.0:8000**

## Wiki 📖

 - Si estamos trabajando con docker compose para lanzar la aplicación podemos configurar algunas variables de entorno en el fichero **docker-compose.yml**. Aparte de las variables de entorno que ofrece la imagen de MySQL, la aplicación de la API y el frontal cuentan con la variablde de entorno **APP_PATH**, la cual indica el directorio raíz de la aplicación (si se modifica esta variable de entorno, también debe hacerse en los respectivos **dockerfiles** de las aplicaciones, tanto en las rutas que aparecen como en el WORKDIR). En el frontal también podemos modificar la url que apunta a la API con **VL_API_URL**

 - Los logs se guardan en todo momento en los directorios **APP_PATH/app/logs/virtual_library_front.log** y **APP_PATH/code/logs/virtual_library_api.log** respectivamente (se puede configurar en el fichero **log_config.yml** situado en el directorio **properties** de cada aplicación)

 - La API se puede consultar en la siguiente dirección: **127.0.0.1:8080/docs/ o 127.0.0.1:8080/redoc**
